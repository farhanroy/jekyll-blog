---
layout: post
title:  "Belajar Bahasa Python 1 : Apa sih bahasa Python itu ?"
date:   2022-01-05 13:08:07 +0700
permalink: /tutorial/python/pengenalan
categories: tutorial python
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-python/python_vDC3ULdXj.webp?ik-sdk-version=javascript-1.4.3&updatedAt=1642148654423"
excerpt: "Python adalah salah satu dari bahasa pemrograman yang saat ini banyak sekali dipakai, python banyak sekali kegunaannnya seperti untuk membangun website, data science, IoT dan masih banyak lagi, Yuk belajar python"
hidden: true
comment: true
---

Python adalah salah satu dari bahasa pemrograman yang saat ini banyak sekali dipakai, python banyak sekali kegunaannnya seperti untuk membangun website, data science, IoT dan masih banyak lagi. 

Python ini dibuat oleh **Guido van Rossum** yang pertama kali dirilis pada tahun 1991. Bahasa ini dirancang agar memudahkan dalam *prototyping*. Python merupakan bahasa yang mudah dipelajari maka dari itu cocok bagi pemula yang ingin belajar pemrograman.

Python ini merupakan interpreter dimana ketika dirunning tidak akan menghasilkan file *executable* atau *.class* seperti pada **Java**. Ketika dirunning python akan langsung mengeluarkan output tanpa file tammbahan. Walau memang ada sisi negatif dari interpreter ini dimana kecepatan eksekusi **relatif** lambat. Tapi ini tidak menjadi masalah karena perbedaannya sangat kecil tidak sampai 1 detik.

Python menerapkan indentasi untuk mengelompokan blok kode, biasanya dengan kurung kurawal atau sintaksis `begin` dan `end`. Dan juga yang menarik di bahasa pemrogrman ini adalah tidak diwajibkannya untuk menggunakan titik koma atau semicolon di akhir lain (Opsional). Meski begitu untuk memisahkan 2 *statement* dalam satu baris maka wajib untuk dipisah dengan titik koma.

Saat ini python menjadi bahasa pemrograman ke-3 paling populer menurut survey yang dilakukan oleh ***stackovervlow.com***. Berada dibawah Javascript dan HTML/CSS.

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-python/image_5c2-T3YBB.png)

 
## Sejarah Python
Python saat ini dikelola oleh  Python Software Foundation (PSF) nirlaba. Namun sebelumnya, GvR disebut Benevolent Dictator for Life (BDFL) karena hampir semua keputusan pengembangan Python dibuat oleh GvR, berbeda dengan bahasa lain yang  menggunakan voting, misalnya. .. Sejak tahun 2000,  beberapa sistem telah dibentuk yang membuat Python  lebih berkelanjutan. Misalnya, Python Extension Proposal (PEP) untuk pengembangan Python dan, tentu saja, Python Software Foundation (PSF). 

 Ketika PSF menjadi organisasi yang mengelola dan mempromosikan Python, PEP menjadi panduan untuk pengembangan Python. Misalnya, beberapa PEP menyertakan bagaimana sintaks dan bahasa Python berkembang, bagaimana modul tidak digunakan lagi, dan sebagainya. Sekitar 30 tahun setelah pengembangan Python, GvR memutuskan untuk berhenti menawarkan BDFL pada 12 Juli 2018.  Salah satu tolok ukur pengembangan Python 
 adalah PEP20, berjudul Zen of Python (https://www.python.org/dev/peps/pep-0020/).

## Mengapa Belajar Python ?

Python adalah bahasa di mana Anda dapat  mengembangkan hampir semua hal. Ini adalah bahasa yang bagus untuk mengembangkan alat sisi server dan kecerdasan buatan serta perangkat lunak  lainnya. Mari kita lihat beberapa alasan mengapa saya belajar Python.

1. Bahasa yang user-friendly 

Sangat mudah untuk menggunakan kode Python. Panduannya yang mudah dan bahasa sederhana memungkinkan siapa saja untuk membuat suatu alat ataupun prototipe. Python membuat seseorang yang sebelumnya tidak menyukai coding pasti akan suka menggunakan Python. Itu adalah salah satu alasan yang kuat sehingga Python bisa menjadi sepopuler saat ini.

2. Mudah dipelajari

Sama mudahnya untuk membangun kode, Python juga mudah dipelajari. Persis seperti bahasa Inggris. Tiga baris kode Java atau C dapat diganti dengan hanya tiga kata kode Python. Python benar-benar gratis dan open source. Tidak perlu ada bahasa rumit untuk dapat menjadi seorang ahli dalam Python.

3. Extensibility dari Python

Python adalah bahasa universal. Tidak ada panduan ketat atau aturan mengekang yang perlu diikuti. Python memungkinkan melakukan semua jenis operasi dengan mudah. Sama sekali tidak perlu memilih platform yang sempurna karena didukung oleh sebagian besar platform mulai dari Windows ke Linux.
