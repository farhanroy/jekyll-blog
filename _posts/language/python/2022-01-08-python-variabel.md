---
layout: post
title:  "Belajar Bahasa Python 3 : Tipe Data dan Variable"
date:   2022-01-05 13:08:07 +0700
permalink: /tutorial/python/tipe-data-dan-variabel
categories: tutorial python
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-python/python_vDC3ULdXj.webp?ik-sdk-version=javascript-1.4.3&updatedAt=1642148654423"
excerpt: "Di tutorial kali ini saya akan membahas tentang tipe data apa saja yang ada di bahasa pemrograman Python dan membuat Variabel menggunakan python"
hidden: true
comment: true
---

Hai temen - temen semua di tutorial kali ini saya akan membahas tentang tipe data apa saja yang ada di bahasa pemrograman **Python** dan membuat Variabel menggunakan python.

## Pengenalan Variable dan Tipe Data

**Variable** adalah sebuah tempat di memori untuk menyimpan sebuah data. Jadi kalau dianalogikan variabel seperti keranjang yang digunakan untuk menyimpan barang, nah barang itu sebuah data. Variable ini bersifat *mutable* artinya data yang ada didalamnya bisa diubah.

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-python/variabel/image_wfUGE-eXI.png)

Kemudian Tipe Data merupakan tipe dari data yang disimpan di dalam variabel. Bisa berupa text, number, atau nilai boolean `true` dan `false`. Cukup simpel apalagi kalau kalian sudah pernah belajar pemrograman sebelumnya

## Membuat Variable di Python

Untuk membuat variable pada bahasa Python tidak perlu menuliskan tipe data atau keyword tambahan cukup nulis nama, dan di asign menggunakan operator sama dengan (=), lalu nilainya.

```python
nama_variabel = nilai
```
Contoh penggunaan
```python
nama = "Roy"
umur = 19
```
Untuk melihat isi variabel maka gunakan fungsi `print()` untuk menampilkan ke console.
```
print(nama)
print(umur)
```
Namun ada aturan dalam menulis sebuah variabel, berikut aturannya:
1. Nama variabel dapat dimulai dengan huruf atau garis bawah (_). 
Contoh: nama, _nama, nama saya, nama_variabel.  

2. Karakter berikut dapat berupa huruf, garis bawah (_), atau angka. 
Contoh: __ nama, n2, nilai1.  

3. Karakter dalam nama variabel peka huruf besar/kecil. Ini berarti peka huruf besar/kecil. Misalnya, variabel_saya dan variabel_saya adalah dua variabel yang berbeda.  

4. Nama variabel tidak boleh menggunakan kata kunci yang sudah ada di Python, seperti: B. jika, sementara, untuk, dll.

## Menghapus Variable
Kalian juga bisa menghapus variabel yang sudah dibuat menggunakan `del()`. Seperti contoh ini
```python
nama = "Roy"
print(nama)
// hapus variabel name
del(nama)
// cetak nama
print(nama)
```
Setelah itu jika kalian sudah menghapus variabel kemudian memanggil variabel tadi maka variabel itu dianggap tidak ada. Sehingga ketika running maka program diatas akan mengeluarkan error seperti ini
```
Traceback (most recent call last):
File "<string>", line 4, in <module>
NameError: name 'nama' is not defined
```
## Tipe Data di Python

Seperti yang saya jelaskan tadi ada beberapa jenis tipe data, secara umum ada 3 tipe data:
1. Number 
2. Teks
3. Boolean (true atau false)

### 1. Tipe Data Number
Tipe data number atau angka dibagi ada 2 jenis:
1.  `int` (Integer): bilangan bulat negatif atau positif. Contoh: 2, 3, -3, 0
2. `float`: bilangan berkoma/pecahan. Contoh: 2.5, 1.5, 2.37

```python
bil_bulat = 90
bil_pecahan = 9.5
```
### 2. Tipe Data Teks
Tipe data teks ini dibagi ada 2 juga:
1. Char: berisi satu karakter. Contoh: 'R', 'a', 'Z
2. String: Berisi 1 sampai lebih karakter. Contoh "Hello"

Ada perbedaan cara penulisan, jika Char maka akan dibungkus dengan petik `'...'`, jika String maka dibungkus dengan petik 2 `"..."`

```python
karakter = 'A'
kalimat = "Hello Bandung"
```

### 3. Tipe Data Boolean
Tipe data ini berisi nilai `True` atau `False` saja. Atau juga bisa diisi dengan **0** sebagai `False` dan **1** sebagai `True` cuman akan diset sebagai `int`.

Contoh:
```python
apakah_benar = False
```

### Mendapatkan Jenis Data
Python mempunyai API atau method `type()` yang bisa digunakan untuk mengecek tipe data dari sebuah variabel.

```python
bil_bulat = 90
bil_pecahan = 9.5
karakter = 'A'
kalimat = "Hello Bandung"
apakah_benar = False

print(type(bil_bulat))
print(type(bil_pecahan))
print(type(karakter))
print(type(kalimat))
print(type(apakah_benar))
```
Program diatas akan menampilkan di konsol seperti ini
```
// Output:
<class 'int'>
<class 'float'>
<class 'str'>
<class 'str'>
<class 'bool'>
```

### Mengganti Tipe Data
Kalian juga bisa mengganti tipe data atau mengkonversinya. Terkadang ada sebuah *case* yang mengharuskan untuk *parse* data ke tipe lain.

Seperti pada program dibawah ini

```python
bil1 = 10
kalimat = str(bil1)
print( type(kalimat) ) // Output: <class 'str'>
```

Berikut method yang bisa digunakan untuk parse data:
1. `int()` untuk mengubah menjadi integer;
2. `long()` untuk mengubah menjadi integer panjang;
3. `float()` untuk mengubah menjadi float;
4. `bool()` untuk mengubah menjadi boolean;
5. `chr()` untuk mengubah menjadi karakter;
6. `str()` untuk mengubah menjadi string.
7. `bin()` untuk mengubah menjadi bilangan Biner.
8. `hex()` untuk mengubah menjadi bilangan Heksadesimal.
9. `oct()` untuk mengubah menjadi bilangan okta.

Nah itu dia tutorial singkat tentang variabel dan tipe data di bahasa pemrograman python

## Referensi
https://www.petanikode.com/python-variabel-dan-tipe-data/
