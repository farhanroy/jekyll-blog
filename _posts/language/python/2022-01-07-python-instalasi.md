---
layout: post
title:  "Belajar Bahasa Python 2 : Instalasi Python di Komputer"
date:   2022-01-07 13:08:07 +0700
permalink: /tutorial/python/instalasi
categories: tutorial python
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-python/python_vDC3ULdXj.webp?ik-sdk-version=javascript-1.4.3&updatedAt=1642148654423"
excerpt: "Tutorial instalasi python di sistem operasi komputer baik, namun karena keterbatasan device disini hanya ada tutorial di Windows saja"
hidden: true
comment: true
---

Untuk menjalankan python perlu juga yang namanya interpreter seperti pada bahasa pemrograman lain. Interpreter python ini bisa dijalankan di banyak OS seperti Linux, Mac, dan Windows. 

Sebelum install ada python dengan versi 2 dan versi 3. Berikut perbedaan dari keduanya:

* Python 2.0 pertama kali dikeluarkan di tahun 2000. Versi terakhirnya, versi 2.7, dikeluarkan di tahun 2010.
* Python 3.0 dikeluarkan di tahun 2008. Versi terbaru saat tulisan ini dibuat, 3.8.2, dirilis pada tanggal 24 Februari 2020.
* Meskipun Python versi 2.7 masih banyak digunakan, namun adopsi Python 3 berkembang sangat cepat.
* Di tahun 2020, Python 2.7 sudah tidak dikembangkan lagi. Dulu saat 2008, sudah diumumkan bahwa Python 2 tidak akan dikembangkan di tahun 2015. Banyak yang pindah, dan banyak yang mengabaikannya.
* Pengembang Python akhirnya menambah waktu hingga tahun 2020. [Ini pemberitahuan resminya](https://www.python.org/doc/sunset-python-2/).

Jadi meskipun baru belajar python saya sarankan memakai yang versi 3 keatas, python versi 2 sering digunakan bagi developer yang masih **bergantung** pada library - library nya.

## Instalasi

Pertama download dulu melalui website ini https://www.python.org/downloads/. Download sesuai dengan OS kalian, secara default web akan mendeteksi OS yang kalian pakai (langsung detek).

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-python/instalasi/image_lBZvHcrLb.png)

Jika kalian menggunakan OS linux, kalian bisa menginstall via terminal. Lakukan perintah dibawah ini, lalu tunggu hingga proses download selesai

```
$ sudo apt-get install python3
```

Setelah kalian menginstall, untuk mengecek apakah sudah terinstall dengan atau belum bisa mencoba untuk melihat versi python, lakukan perintah dibawah ini

```
$ python3 --version

// Output Python 3.8.2
```

## Mencoba Running Python

Sudah menjadi tradisi bahwa program Hello World sebagai prorgam pertama untuk belajar 😄, Oke folder kosong terlebih dulu, lalu gunakan software **VS Code** untuk membuka folder tersebut. Kemudian buat sebuah file dengan nama **hello.py**

> Note: Setiap program python di tulis dengan file yang memiliki format .py

Buka file nya lalu ketik-an kode dibawah ini, untuk menampilkan text **"Hello World"**

```python
print("Hello World")
```

Lalu untuk merunning file tersebut maka buka terminal dengan perintah berikut

```
$ python3 hello.py
```



## Online IDE

Selain itu kalian juga bisa menggunakan tools online untuk menjalankan kode python. Ada banyak pilihan disini saya ambil contoh di situs https://www.programiz.com/python-programming/online-compiler/.

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-python/instalasi/image_1__AeCoq8d5r.png)
