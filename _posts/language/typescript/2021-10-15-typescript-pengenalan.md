---
layout: post
title:  "Belajar Bahasa Pemrograman Typescript untuk Pemula: Pengenalan"
date:   2021-10-11 13:08:07 +0700
permalink: /tutorial/typescript/pengenalan
categories: tutorial
categories: tutorial typescript
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-typescript/belajar_bahasa_typescript_pXClGbbkx?updatedAt=1634266582162"
excerpt: Berkenalan dengan bahasa pemrograman typescript untuk pemula, bahasa ini banyak digunakan untuk pemengembangan aplikasi frontend
---

Pada artikel kali ini, tibalah di kategori belajar bahasa pemrograman `typescript` untuk pemula, jadi jangan khawatir jika kalian baru belajar pemrograman.

## Apa itu Typescript ?

Mungkin banyak yang bertanya - tanya typescript itu bahasa apa ? apakah bedanya dengan javascript ?

Dilansir dari typescript.org, Typescript adalah **"TypeScript is a strongly typed programming language which builds on JavaScript giving you better tooling at any scale."** kalau dibahasa indonesiakan artinya `Typescript` bahasa pemrograman yang dibangun di atas **JavaScript** yang menyediakan tools yang lebih baik di skala apa pun (scalable). 

![](https://sec.ch9.ms/ch9/B0BE/C77D5232-4D38-474D-80A5-9ED20166B0BE/AnHourWithAndersHejlsberg_512_ch9.jpg)

Typescript saat ini sudah banyak sekali digunakan oleh developer, karena banyak sekali fitur yang disediakan yang tidak dimiliki javascript, ada satu artikel yang menyebutkan bahwa TypeScript merupakan salah satu bahasa pemrograman yang paling banyak digunakan.Bahasa pemrograman ini dirancang oleh Andres HejlsBerg salah satu engineer Microsoft yang juga merupakan designer bahasa pemrograman **C#**. Rilis awal pada tahun 2012. 

Hal terbaik tentang TypeScript adalah Anda juga akan mendapatkan akses ke fitur-fitur dari versi terbaru ECMAScript. Dan Anda juga dapat menggunakan fitur di luar cakupan itu. Saat Anda mengompilasi TypeScript, Anda akan dapat menghasilkan versi JavaScript yang aman di semua platform.

## Bagaimana cara kerjanya ?

TypeScript membutuhkan compiler yang dapat mengubah sintaks TypeScript menjadi JavaScript standar. Dan kompiler ini disebut transpiler. Transpiler dirancang untuk mengubah satu bahasa pemrograman menjadi bahasa lain.

Kalian akan melihat bahwa file TypeScript datang dengan ekstensi .ts. Setelah transpiler mengkompilasi file .ts, Anda akan mendapatkan file .js sebagai output.

## Instalasi

Untuk instalasi nya ada 2 cara kalian bisa memakai kompiler online untuk sekedar belajar saja di [typescriptlang.org/play](https://typescriptlang.org/play). Jika kalian pengen running di device kalian sendiri, maka harus menggunakan **npm**. Kalau kalian belum punya npm kalian harus install dulu caranya [disini](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm/).

>Note: selain npm juga bisa pake yarn

Setelah itu install plugin nya, dengan menggunakan sintaks berikut:

```sh
npm install typescript --save-dev
```
Jika berhasil maka terminal akan menampilkan seperti ini

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-typescript/pengenalan/Screen_Shot_2021-10-15_at_14.41.33_buDrVWLHY.png?updatedAt=1634284997759)

Kemudian kalian dapat menjalankan kompiler TypeScript menggunakan salah satu dari perintah berikut:

```
npx tsc <nama_file>.ts
```
Oke langsung saja kita ke contoh pengimplementasian pada project sebenarnya, untuk ke depannya saya menggunakan kompiler online karena lebih cepat dan simple.

## Hello World
![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-typescript/pengenalan/image_wKspWI6QM.png?updatedAt=1634638942477)

## Referensi
- https://glints.com/id/lowongan/typescript-adalah/#.YWkjqdlBzvU