---
layout: post
title:  "Belajar Bahasa Pemrograman Go untuk Pemula: Pengenalan"
date:   2021-10-11 13:08:07 +0700
permalink: /tutorial/go/pengenalan
categories: tutorial
categories: tutorial go
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-go/belajar_bahasa_kotlin_1_pengeNALAN_KOTLIN_kqLecLXsy.png?updatedAt=1634266485225"
excerpt: Berkenalan dengan bahasa pemrograman go untuk pemula, bahasa ini banyak digunakan untuk pemengembangan aplikasi backend
---

Jadi apa sih bahasa pemrograman Go itu ? 

Menurut dilaman resminya bahasa Go atau biasa disebut **golang** di golang.org sebagai berikut

> Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.

Go adalah bahasa pemrograman berifat open source dan memudahkan untuk membuat sebuah software yang **Sederhana**, **Andal**, dan **Efisien**. Karena open source, kita bisa ikut berkontribusi ke bahasa ini melalui github golang di https://github.com/golang. Bahasa ini memiliki tekstur prosedural jadi kalian nggak akan nemu konsep OOP disini, mungkin kalau ada update lagi ada go yang make OOP.

Sekarang banyak sekali perusahaan yang sudah menerapkan golang sebagai stack untuk pengembangan aplikasi disisi backend nya. Menurut beberapa sumber katanya `golang` dibuat oleh **google**, apakah benar ? Oke mari kita bahas lebih lanjut.

## Sejarah

Sejarah dari golang pertama kali dikembangkan di Google pada tahun `2007` an oleh ***Ken Thompson***, ***Rob Pike***, dan ***Robert Griesemer***. Dan baru dirilis ke publik pada tahun `2009`. Dalam pembuatannya bahasa go didasarkan kepada bahasa C tujuannya untuk mengurangi "extraneous garbage" atau sampah asing pada kode seperti pada C++. 

Menurut mas Eko Kurniawan di video youtube nya salah satu yang membuat bahasa golang ini menjadi hype seperti sekarang ini adalah container Docker yang di built dari bahasa go, hal ini yang membuat para developer menjadi lebih yakin dengan performa dari `golang` ini.

Nah itu dia sejarah perkembangan singkat bahasa pemrograman golang, kalau dilihat dari umurnya golang termasuk masih muda ketimbang dengan umur bahasa pemrograman lain seperti python, php, atau java.

## Kenapa harus go ?
 
Mungkin banyak yang bertanya, kenapa sih belajar go kan sudah ada nodejs atau php ?

Dikutip dari [glints.com](https://glints.com/id/lowongan/belajar-golang/#.YWZxmtlBzvU), ada beberapa alasan mengapa Golang berikut listnya:
 
1. Membangun aplikasi web yang aman dan scalable
2. Membangun sistem kompleks yang membutuhkan kinerja tinggi
3. Kode server jaringan, terutama server web dan layanan mikro
4. Membangun tim developer yang scalable, baik di startup maupun perusahaan korporat
5. Membangun dan membuat sistem cloud computing yang scalable

Nah dari alasan alasan itulah banyak backend engineer yang mulai menggunakan golang sebagai bahasa untuk mengembangkan aplikasi backend mereka.

## Kelebihan dan Kekurangan

Golang memiliki kelebihan dan juga kekurangan, semua bahasa pemrograman pasti memiliki dua hal ini

### Kelebihan:
1. bisa meningkatkan performa dan menarik lebih banyak pengunjung aplikasi
2. Sintaks nya yang sederhana membuat mudah dipelajari dan dibaca
3. Memberi tahu jika ada pengetikan yang salah selama proses kompilasi
4. Tidak seperti C atau C++, Golang memiliki garbage collector
5. Yang pasti **Open Source**

### Kekurangan

1. Membutuhkan fungsi tertentu untuk mengembalikan error jika sebenarnya ada error yang diharapkan
2. Tidak adanya manajemen memori manual
3. Keamanan runtime belum sebaik Ruby

## Resource belajar golang

Selain di blog saya banyak sekali resource untuk belajar golang, bisa dari youtube, online course, atau web tutorial. Saat ini sudah banyak web tutorial pemrograman golang yang berbahasa indonesia. Berikut list sumber yang bisa kalian gunakan:

#### 1. Web Programmer Zaman Now: Belajar Go-Lang untuk Pemula (Youtube)

Ini salah satu playlist video youtube untuk belajar bahasa pemrograman golang. Video ini dibuat oleh mas Eko Kurniawan, video ini cocok untuk pemula dan saya kira cukup lengkap

👉 https://www.youtube.com/playlist?list=PL-CtdCApEFH_t5_dtCQZgWJqWF45WRgZw

#### 2. Dasar Pemrograman Golang (Website)

Ini merupakan website tutorial berbahasa **indonesia** buatan mas Noval Agung materinya juga lengkap dan juga disertai kode program tutorial yang ada di github. Langung...

👉 https://dasarpemrogramangolang.novalagung.com/

#### 3. Master Go (Golang) Programming:The Complete Go Bootcamp 2021

Nah jika kalian punya sedikit rezeki, ada juga course udemy belajar golang yang sesuai namanya **Complete**. Namun course ini berbahasa inggris, tapi itu bukan menjadi masalah, yang penting tetep istiqomah dan semangat untuk belajar

👉 https://www.udemy.com/course/master-go-programming-complete-golang-bootcamp/

Selain itu jika kalian pengen gabung komunitas, diindonesia ada komunitas golang via telegram t.me/golangID

Jadi kesimpulannya golang ini awalnya dibuat oleh engineer google dan bahasa ini relatif muda, dikembangkan diatas bahasa C yang memiliki beberapa kelebihan.

## Referensi
- https://glints.com/id/lowongan/belajar-golang/#.YWV4ntlBzvU
