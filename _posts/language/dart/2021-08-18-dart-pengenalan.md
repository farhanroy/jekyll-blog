---
layout: post
title:  "Belajar bahasa Dart: Pengenalan"
date:   2021-08-16 13:08:07 +0700
permalink: /tutorial/dart/pengenalan
categories: tutorial dart
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-dart/dart_-MPLWCd80.webp?updatedAt=1630208906702"
excerpt: "Variable sebagai konsep dasar yang sangat penting untuk mempelajari dart"
---

# Apa itu variabel ?
Oke teman - temen semua, kali ini kita sampai pada sesi berkenalan dengan dengan variabel. Jika teman - teman pernah belajar matematika pasti pernah mendengar istilah variabel, yang mana di dalam ilmu matematika.
###
Variabel adalah lambang pengganti (yang umumnya adalah huruf) suatu bilangan yang belum diketahui 
###
```contohnya : x + y = 10```
###
Lantas apa itu variabel di dalam bahasa pemrograman, dan bagaimana cara membuat nya di dalam bahasa dart.

Bayangkan teman - teman mempunyai sebuah kotak yang digunakan untuk menyimpan sebuah benda dan teman - teman bisa menamai kotak tersebut supaya mudah untuk mencarinya. Itulah pengertian sederhana dari variabel, jadi variabel adalah suatu tempat untuk menyimpan data didalam memori yang bisa diubah selama program tersebut dijalankan.

## Membuat variabel di dart
Oke, setelah kita membahas pengertian apa itu variabel, kita akan mencoba membuat variabel di bahasa dart Tetapi sebelum kesana, mari kita lihat dulu bagaimana variabel itu disusun.
Jadi gambar di atas merupakan komposisi susunan dari sebuah variabel. Dan jika teman - teman mau membuat sebuah variabel maka teman - teman harus mendefinisikan dulu tipe data nya, lalu nama variabel kemudian isi dari sebuah variabel tersebut.

Sekarang, mari kita praktekkan apa yang sudah kita pelajari, silahkan teman - teman buka https://dartpad.dev/ (IDE Online), lalu di dalam function main() tuliskan kode seperti ini


<div align="center">
<iframe
  src="https://carbon.now.sh/embed?bg=rgba%28171%2C+184%2C+195%2C+1%29&t=material&wt=none&l=dart&ds=true&dsyoff=20px&dsblur=68px&wc=true&wa=true&pv=56px&ph=56px&ln=false&fl=1&fm=Hack&fs=14px&lh=133%25&si=false&es=2x&wm=false&code=var%2520nama%2520%253D%2520%2522Roy%2522%253B%2520%252F%252F%2520Tipe%2520data%2520inference%250A%250Aint%2520umur%2520%253D%252017%253B%2520%2520%252F%252F%2520commonly%2520data%2520type%250A%250Adynamic%2520alamat%2520%253D%2520%2522pasuruan%2522%253B%2520%252F%252F%2520dynamic%2520type%2520data%250A%250A%250Anama%2520%253D%2520%2522Farchan%2522%253B%250A%250Aprint%28nama%29%253B%2520%252F%252F%2520print%2520-%253E%2520Farchan"
  style="width: 563px; height: 366px; border:0; transform: scale(1); overflow:hidden;"
  sandbox="allow-scripts allow-same-origin">
</iframe>
</div>

Dan silahkan teman - teman jalankan, dan kode dijalankan tanpa ada error yang terjadi. Apakah sudah selesai ?, owh tentu belum.., 
###
coba teman - teman perhatikan variabel alamat yang mempunyai tipe data dynamic, dimana awal nya diisi sebuah kalimat / kata, kemudian diisi lagi dengan angka ?, kenapa bisa terjadi ?, 
###
itu menjadi PR bagi teman - teman semua yah, silahkan di googling dengan keyword ‘dynamic type data in dart’. Dan juga sekarang coba teman - teman ganti kata umur di variabel umur menjadi class atau var, silahkan dicoba dijalankan lagi, maka terjadi error?, kenapa terjadi error ?, sekali lagi itu jadi PR untuk teman - teman semua, silahkan di googling dengan keyword ‘rules naming variable in dart’. 
