---
layout: post
title:  "Belajar bahasa Dart: Collection"
date:   2021-08-20 13:08:07 +0700
permalink: /tutorial/dart/collection
categories: tutorial dart
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-dart/dart_-MPLWCd80.webp?updatedAt=1630208906702"
excerpt: "Collection adalah tipe data pada dart untuk alternatif dari array"
---


Bahasa pemrograman **dart** tidak seperti bahasa pemrograman lainnya. Dart tidak men-support array. Tetapi, dart menyediakan tipe data alternatif dari array yaitu *Collection* yang akan kita akan pelajari kali ini. library ``dart:core`` menyediakan tipe data ini.

Ada 4 macam dari tipe data Collection:
* List
* Map
* Set (Tidak akan kita bahas)
* Queue (Tidak akan kita bahas)

## List
List sederhananya sebuah mekanisme untuk menyimpan data sekaligus yang paling umum digunakan, List menggunakan pendekatan index. Jadi jika kita ingin mengakses atau memanipulasi item atau value yang ada pada List maka kita harus menyertakan index ke berapa.

👉 List didalam dart ada 2 bagian:

- **Fixed Length List**
###
List yang memiliki panjang item tetap, artinya kita tidak bisa mengubah pada saat proses runtime atau ketika aplikasi dijalankan
##
<div align="center">
<iframe
  src="https://carbon.now.sh/embed?bg=rgba%2821%2C45%2C45%2C1%29&t=cobalt&wt=none&l=dart&ds=true&dsyoff=20px&dsblur=68px&wc=true&wa=true&pv=56px&ph=56px&ln=false&fl=1&fm=Hack&fs=14px&lh=133%25&si=false&es=2x&wm=false&code=var%2520fixedList%2520%253D%2520List%283%29%253B%250A%250AfixedList%255B0%255D%2520%253D%252010%253B%2520%252F%252F%2520Isi%2520list%2520pada%2520index%2520ke%25200%2520dengan%2520nilai%252010%250A%250AfixedList%255B1%255D%2520%253D%252020%253B%2520%252F%252F%2520Isi%2520list%2520pada%2520index%2520ke%25201%2520dengan%2520nilai%252020%250A%250AfixedList%255B2%255D%2520%253D%252030%253B%2520%252F%252F%2520Isi%2520list%2520pada%2520index%2520ke%25202%2520dengan%2520nilai%252030%250A%250A%252F%252F%250AfixedList%255B3%255D%2520%253D%252040%253B%2520%252F%252FERR%253A%2520out%2520of%2520index%250A%250Aprint%28fixedList%255B0%255D%29%253B%2520%252F%252F%252010%250Aprint%28fixedList%255B3%255D%29%253B%2520%252F%252F%2520ERR%250A"
  style="width: 628px; height: 492px; border:0; transform: scale(1); overflow:hidden;"
  sandbox="allow-scripts allow-same-origin">
</iframe>
</div>
Dari kode diatas menegaskan bahwa jika Fixed Length List tidak bisa ditambah item nya jika melebihi jumlah item yang sudah ditentukan

##

- **Growable List**
###
Ini merupakan kebalikan dari ***Fixed Length List***, yang mana List tipe ini tidak memiliki panjang item tetap, artinya kita bisa mengubah pada saat proses runtime atau ketika aplikasi dijalankan
<div align="center">
<iframe
  src="https://carbon.now.sh/embed?bg=rgba%28245%2C166%2C35%2C1%29&t=cobalt&wt=none&l=dart&ds=true&dsyoff=20px&dsblur=68px&wc=true&wa=true&pv=56px&ph=56px&ln=false&fl=1&fm=Hack&fs=14px&lh=133%25&si=false&es=2x&wm=false&code=var%2520growList%2520%253D%2520%255B%255D%253B%250A%250AgrowList.addAll%28%255B10%252C%252020%252C%252030%252C%252040%252C%252070%252C%2520100%255D%29%253B%250A%250A%250Aprint%28fixedList%255B0%255D%29%253B%2520%252F%252F%252010%250Aprint%28fixedList%255B3%255D%29%253B%2520%252F%252F%252040%250A"
  style="width: 513px; height: 330px; border:0; transform: scale(1); overflow:hidden;"
  sandbox="allow-scripts allow-same-origin">
</iframe>
</div>
Sebaliknya di Growable list kita bisa menambahkan jumlah item sesuka hati karena di list ini, kita tidak mematok jumlah jenis listnya.

## Map
Hampir sama dengan List tetapi Map tidak menggunakan pendekatan index tapi menggunakan pendekatan berupa 
``key value``. Jadi kalau kita ingin mengakses atau memanipulasi item atau value yang ada pada **Map** maka kita harus memanggil key valuenya
<div align="center">
<iframe
  src="https://carbon.now.sh/embed?bg=rgba%28245%2C166%2C35%2C1%29&t=cobalt&wt=none&l=dart&ds=true&dsyoff=20px&dsblur=68px&wc=true&wa=true&pv=56px&ph=56px&ln=false&fl=1&fm=Hack&fs=14px&lh=133%25&si=false&es=2x&wm=false&code=var%2520someMap%2520%253D%2520%257B%250A%2520%2520%2522nama%2522%2520%253A%2520%2522Andi%2522%252C%250A%2520%2520%2522umur%2522%2520%253A%252020%250A%257D%253B%250A%250A%252F*%250A%2509Panggil%2520item%2520dari%2520map%250A*%252F%250A%250A%250Aprint%28someMap%255B%2522nama%2522%255D%29%253B%2520%252F%252F%2520andi%250Aprint%28someMap%255B%2522umur%2522%255D%29%253B%2520%252F%252F%252020%250A"
  style="width: 411px; height: 420px; border:0; transform: scale(1); overflow:hidden;"
  sandbox="allow-scripts allow-same-origin">
</iframe>
</div>

Dari kode diatas key value pada variable ``someMap`` ada 2 yaitu, **nama** dan **umur** yang mana masing masing punya value sendiri.

####

Cara memanggilnya pun cukup mudah, kita hanya perlu memanggil ``key value`` keinginan kita. Ini akan sangat
berguna ketika kita menangani data yang berbentuk JSON.
