---
layout: post
title:  "Belajar bahasa Dart: Operator aritmatika"
date:   2021-08-30 12:08:07 +0700
permalink: /tutorial/dart/operator-aritmatika
categories: tutorial dart
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-dart/dart_-MPLWCd80.webp?updatedAt=1630208906702"
excerpt: "Penerapan perhitungan aritmatika pada bahasa dart"
---
Pada artikel ini, saya akan membahas mengenai operator aritmatika. Operator digunakan untuk memproses atau memanipulasi data. Operator pada dart hampir sama dengan operator - opearator dalam bahasa pemrograman lainnya.

## Introduction
Sebelum mempelajari tentang operator dalam bahasa ini, lebih baik kalian pelajari dulu istilah - istilah penting tentang operator. Istilah - istilah itu adalah **Operand**, **Ekspresi**, dan **Operator**. **Operand** adalah suatu data/value yang akan di operasikan, seperti contoh: 2 * 2, angka 2 pada soal matematika tersebut disebut sebagai operand.
**Ekspresi** adalah jenis pernyataan khusus untuk mengevaluasi beberapa nilai, seperti contoh soal matematika sebelumnya, itu sebuah ekspresi. Sedangkan **Operator** sebagai simbol untuk mengoperasikan operand. Terdengar *njelimet*, baik saya berikut diagram mengenai operator:

![](https://ik.imagekit.io/u8uufhbnoej/blog/belajar-bahasa-dart/operator-aritmatika/diagram_operator_JgfYi8skMMc.png)

## Jenis operator aritmatika
Operasi aritmatika adalah operasi yang mendukung penjumlahan, pengurangan, perkalian, dan pembagian. Pada pemrograman dart, ada beberapa jenis operasi aritmatika yang bisa dilakukan. Berikut tabel lengkap macam macam operasinya :
| Operator      | Fungsi |
| ----------- | ----------- |
| +      | Penjumlahan dua operand       |
| -   | Operasi pengurangan        |
| *      | Mengalikan operand       |
| /   | Pembagian antara dua operand        |
| %      | Mencari sisa hasil bagi       |
| -expr   | Membalikkan tanda ekspresi (unary minus)        |
| ~/      | Membagikan operand pertama dengan operand kedua dan membalikkan nilai integer       |

Baik, langsung saja kita buat studi kasus berupa mengoperasikan 2 buah operand. Operator 1 bernilai **10** dan operator 2 bernilai **6**. Disini saya menggunakan IDE online di https://dartpad.dev

<div>
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/belajar-bahasa-dart/operator-aritmatika/operator_aritmatika_7JnfW726fk_.png">
</div>

Beberapa operator mungkin belum pernah kalian jumpai sepert **~/**. Maksud datri operator **~/** adalah membulatkan hasil dari pembagian kedua operand. Misal 5 **/** 2  maka hasilnya 2.5. Dengan menggunakan operator **~/** hasilnya hanya 2 saja karena dibulatkan.

## Increment dan Decrement
Selain operator aritmatika diatas, ``Dart`` juga mendukung increment dan decerement. Bagi kalian yang belum pernah dengar istilah ini, **Increment** adalah menambah 1 angka pada variable dengan simbol **++** sedangkan Decrement adalah mengurungi 1 angka pada variable dengan menggunakan simbol **--**. Berikut tabel lengkapnya

| Operator |	Maskutnya |
|----------|------------|
|++var	|var = var + 1 |
|var++	|var = var + 1 |
|--var	|var = var – 1 |
|var--	|var = var – 1 |

Disini ada perbedaan mengenai peletakan simbol yaitu ada yang diawal dan diakhir. Untuk yang diawal dinamakan **pre** dan untuk yang diakhir dinamakan **post**
### Pre
Pada **pre** kompiler akan memproses terlebih dahulu variable baru bersamaan dengan menambah 1 atau menguranginya 1. Seperti contoh kode berikut:
```dart
void main() {
  var nilai = 2;
  print(++nilai);
  print(--nilai);
}
//Output 
3
2
```
### Post
Berbeda dengan **post** kompiler akan memproses terlebih dahulu variable baru kemudian menambah 1 atau menguranginya 1. Seperti contoh kode berikut:
```dart
void main() {
  var nilai = 2;
  print(nilai++);
  print(nilai);
}
//Output 
2
3
```
## Penutup
Itu dia penjelasan tentang operator aritmatika pada bahasa pemrograman ``dart``. Materi operator tentun sangat penting, banyak bahasa pemrograman yang mempunyainya mengingat keberadaanya sangat dibutuhkan. Sebenarnya masih ada lagi tentang operator pada ``dart``. Saya akan membahasnya di artikel selanjutnya.
 
Seperti biasa, jika kalian masih belum faham tentang penjelasan saya silahkan kunjungi situs resminya di https://dart.dev/guides/language/language-tour#arithmetic-operators

Sampai jumpa :)

## Referensi
- https://belajarflutter.com/dart-operator/
- https://dart.dev/guides/language/language-tour#arithmetic-operators
