---
layout: post
title:  "Belajar bahasa Dart: Operator logika"
date:   2021-08-30 12:08:07 +0700
permalink: /tutorial/dart/operator-logika
categories: tutorial dart
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-dart/dart_-MPLWCd80.webp?updatedAt=1630208906702"
excerpt: "Membandingkan dua variabel menggunakan operator logika di bahasa pemrograman dart"
draft: false
---

Di artikel ini saya akan menjelaskan penggunaan operator logika pada bahasa pemograman `Dart`. Sebelum itu, sebaiknya kita kenali apa sih operator logika itu ?

Jadi operator logika adalah operator yang melakukan operasi logika seperti logika AND dan logika OR, operator logika digunakan untuk menghasilkan nilai boolean true atau false dari 2 kondisi atau lebih.

## Jenis - jenis operator logika

Berikut ini tabel dari operator logika:

|Operator|Nama operator|Detail|
|--------|-------------|------|
|!|*Logical* **NOT**|Menegasikan keadaan pada operand, jadi jika operand bernilai true maka akan bernilai false|
|&&|*Logical* **AND**|Jika kedua operand true, maka hasilnya true.|
| \|\| |*Logical* **OR**|Apabila salah satu dari 2 operandnya true, maka menghasilkan output true.|

Biasanya operator logika ini digunakan untuk menggabungkan beberapa hasil operasi perbandingan.

## Contoh program
Baik kita langsung saja ke contoh program. Yang pertama contoh penggunaan operator **NOT**.
```dart
void main() {
    bool isMale = true;
    print(!isMale); // Output: false
}
```
Program diatas memiliki variabel `isMale` bertipe *boolean* dan berisi nilai **true**. Ketika di print out, ditambahkan operator not sebelum pemanggilan variabel `isMale`. Ketika di running maka akan menampilkan nilai **false** karena variabel bernilai true yang dinegasikan melalui operator NOT.

Contoh kedua menerapkan operator AND, dimana jika salah satu nilai bernilai false maka hasilnya juga akan false alias harus true semua.

```dart
void main() {
    var A = true;
    var B = false;

    bool isSame = A && B

    print(isSame); // Output: false
}
```

Output dari program diatas adalah **false** karena variabel A dan B tidak memiliki value yang sama melainkan berbeda (true dan false).

Contoh ketiga ini merupakan penerapan dari operator OR, jika salah datu dari kedua operand bernilai true maka  hasilnya akan true.

```dart
void main() {
    var A = true;
    var B = false;

    bool isSame = A || B

    print(isSame); // Output: true
}
```
Output dari program diatas adalah **true** karena salah satu dari operand bernilai **true** yaitu variabel A

Nah contoh yang terakhir ini gabungan dari beberapa operator logika, jadi operator logika sama dengan aritmatika, dimana operasi yang ada didalam kurung akan dijalankan terlebih dulu.

```dart
void main() {
    bool hasil;

    hasil = (false && true) || (true || false);
    print(hasil);

    hasil = !false && (false || true);
    print(hasil);

    hasil = ((true && true) || (true || false)) && !true;
    print(hasil);
}
```
Output di operasi yang pertama, adalah true karena operasi (false && true) || (true || false) akan diproses menjadi false || true, hasilnya **true**.

Output pada operasi yang kedua, adalah true juga karena operasi !false && (false || true) akan diproses menjadi true && true, hasilnya **True**.

Output di operasi yang terakhir adalah false kenapa ? karena operasi ((true && true) || (true || false)) && !true akan diproses menjadi (true || true) && false, kemudian menjadi true && false, hasilnya **false**.

## Kesimpulan
Kesimpulannya operasi logika ini membandingkan kedua operand menggunakan operator logika yang sudah dijelaskan diatas, apakah hasil dari operasi itu false atau true. Operasi ini akan banyak digunakan mungkin di flutter biasa digunakan di `CheckBox`, fungsinya membandingkan checkbox satu dengan lain apakah tercentang semua atau salah satu.

## Referensi

- https://belajarflutter.com/penjelasan-tentang-operator-logika-pada-dart/