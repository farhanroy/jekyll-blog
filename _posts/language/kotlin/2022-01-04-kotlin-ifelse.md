---
layout: post
title:  "Belajar bahasa Kotlin: Percabangan if, if-else, if-else-if"
date:   2021-08-16 13:08:07 +0700
permalink: /tutorial/kotlin/percabangan-if
categories: tutorial kotlin
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/kotlin_nkK7BWb0U.webp?updatedAt=1630209054609"
excerpt: "Variable menjadi elemen terpentng dalam mempelajari suatu bahasa pemrograman. Itu terjadi karena variable menjadi wadah untuk menyimpan data, yang nantinya data tersebut dapat digunakan ketika program berjalan atau running."
hidden: true
comment: true
---

Saat mengembangkan suatu program pasti akan bertemu dengan alur dimana memerlukan sebuah kondisi untuk menjalankan sebuah *statement* atau *expression*. Seperti ketika mengecek apakah sebuah variabel itu genap atau ganjil, jika ganjil lakukan apa dan jika genap lakukan apa.

Konsep seperti ini biasa disebut dengan **Percabangan**. Hampir seluruh bahasa pemrograman memiliki konsep semacam ini. Tak terkecuali dengan `Kotlin`. Pada Kotlin ada 2, menggunakan **if** dan menggunakan **when**. Berikut struktur dasar dari percabangan if

```
if (kondisi) {
    kerjakan
}
```

Yang kali ini kita bahas adalah Percabangan **if**

## If Expression
Pertama adalah **If Expression**. Ini menggunakan kata kunci atau keyword `if`. if akan menguji sebuah kondisi atau *statement* jika kondisi terpenuhi maka akan menjalakan proses. Biasanya kondisi berisi dengan operator perbandingan dimana akan menghasilkan nilai **True** atau **False**.

Misal pada contoh kode dibawah ini:

```kotlin
val bilangan = -10

if (bilangan < 0) {
    println("Bilangan Negatif")
}

// Output = Bilangan Negatif
```

Program diatas akan mengecek apakah suatu bilangan itu positif atau negatif, jika bilangan itu kurang dari 0 maka dikatakn bilangan itu negatif. Nah terus gimana kalau bilangan itu positif ? gimana memberi aksi atau proses nya ?

Jawabannya adalah dengan menggunakan `else`. Pemaparannya ada dibawah ini

## If-else Expression
Untuk memberi aksi ketika kondisi tidak terpenuhi maka gunakan `else`. else ini akan dijalankan ketika kondisi pada if tidak terpenuhi. Berikut contoh kode yang merupakan kelanjutan dari kode diatas.

```kotlin
val bilangan = -10

if (bilangan < 0) {
    println("Bilangan Negatif")
} else {
    println("Bilangan Positif")
}

// Output = Bilangan Negatif
```

Selanjutnya adalah untuk membuat 2 kondisi maka gunakan keyword `else if`.

## If-else-if Expression
Membuat 2 kondisi bukan berarti 2 `if` tapi menggunakan keyword `else if`. Else if terletak setelah penulisan block if, jadi berada di tengah tengah if dan else. Jadi ketika kondisi pertama tidak terpenuhi maka akan ke kondisi kedua dan juga masih tidak terpenuhi maka berarti lanjut ke block `else`.

Seperti pada kode berikut:

```kotlin
val jam = 16;

if (jam == 12) {
    println("Waktunya makan siang")  
} 
else if (jam == 16) {
    println("Waktunya mandi")  
}
else {
    println("Tidak ada")  
}

// Output = Waktunya mandi
```

Ketika jam tidak memenuhi kondisi `if` maka akan dilanjutkan ke `else if`, ketika memenuhi maka akan dijalankan perintah `println()`. Nah kalau ada pertanyaan kapan menggunakan else if ? maka ketika diperlukan kondisi yang lebih dari 1 maka gunakan `else if`.

Namun untuk yang memang banyak memerlukan kondisi misal 10 kondisi maka gunakan percabangan `when`, percabangan ini akan saya bahas di artikel Tutorial Kotlin selanjutnya.

## Referensi