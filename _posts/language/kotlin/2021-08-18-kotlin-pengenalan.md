---
layout: post
title:  "Tutorial Kotlin 1 : Belajar Kotlin, Pengenalan Kotlin untuk Pemula"
date:   2021-12-25 13:08:07 +0700
permalink: /tutorial/kotlin/pengenalan
categories: tutorial kotlin
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/kotlin_nkK7BWb0U.webp?updatedAt=1630209054609"
excerpt: "Kotlin merupakan bahasa pemrograman yang biasa digunakan untuk mengembangkan aplikasi berbasis Android"
hidden: true
comment: true
---

Halo temen - temen semua, kali ini saya akan membagikan tentang tutorial bagaimana menggunakan bahasa pemrograman **Kotlin**. Artikel ini merupakan bagian pertama dari list belajar Kotlin, sebelum lebih dalam mempelajari kotlin yuk kenalan dulu dengan bahasa ini !

`Kotlin` merupakan bahasa pemrograman yang dibuat oleh Jetbrain. Ya, perusahaan pembuat berbagai IDE terkenal seperti Pycharm (untuk pengembangan python), Goland (untuk pengembangan Go), dan Intellij IDEA yang akan kita gunakan dalam tutorial selanjutnya.

**Kotlin** adalah bahasa pemrograman yang bersifat *Open Source* seperti Javascript, C, Go, dan lain sebagainya. Bahasa ini termasuk kedalam bahasa pemrograman tingkat tinggi yang lebih mudah dipahami oleh manusia. Kotlin berjalan diatas JVM dan berorientasi objek. Cuma untuk running biasa bisa tanpa membuat class seperti pada Java.

## Sejarah

`Kotlin` pertama kali rilis pada Juli 2011. Kotlin dikembangkan selama 1 tahun oleh **Dmitry Jemerov**, penulis buku “Kotlin in action” bersama dengan **Svetlana Isakova**. Nama kotlin sendiri berasal dari pulau Kotlin di dekat **St. Petersburg**.  Kotlin menjadi bahasa pemrograman ke 3 yang didukung android selain `Java` dan `C++`. Sejak rilis pada September 2018 yaitu versi 1.3 kotlin memiliki 2 ekstensi file yaitu .kt dan .kts.

Pada saat even Google I/O 2017, google merilis kotlin sebagai bahasa pemrograman dalam pengembangan aplikasi android selain Java dan C++. Karena kotlin di klaim memiliki banyak keunggulan dibandingkan dengan bahasa Java yang bisa mempercepat dalam proses pengkodean.

Saat ini sudah banyak API android yang sudah dibuat dengan bahasa Kotlin, salah satunya Jetpack Compose. Library ini dibuat dengan kotlin dan tidak menggunakan Java lagi seperti **Android View**. 

## Kelebihan

Berikut beberapa kelebihan yang ada pada bahasa Kotlin:

1. Lebih ringkas dari pada java

Ini merupakan alasan utama, kenapa menggunakan bahasa kotlin dari pada bahasa java, karena sintaks yang biasanya di tulis dengan Java bisa lebih dari 10 baris maka dengan kotlin hanya 5 baris saja. Salah satunya penggunaan setter getter seperti berikut:

Pada java setter getter akan seperti ini:
```java
class User {
    private String username;
    private String password;

    // ini method setter
    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    // ini method getter
    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }
}
```

sedangkan ketika menggunakan Kotlin, programmer bisa menggunakan yang namanya `data class`

```kotlin
data class User(val username: String, val password: String)
```

2. Dapat beroperasi bersama Java
Walaupun berbeda kotlin juga bisa membuat objek dari java dan sebaliknya. Jadi misal begini, ada sebuah class di tulis dalam bahasa Java, terus objek nya dibuat di kotlin itu bisa dan tanpa harus ada cara tambahan.

```
// Java
class Mobil{}

// Kotlin
val mobil = Mobil()
```
Maka dari itu android bisa diprogram dengan bahasa Kotlin sedangkan banyak API nya menggunakan bahasa Java, dan itu tidak jadi masalah karena Kotlin bisa beroperasi bersama Java.

3. Mudah dipelajari
Yang ketiga adalah bahasa Kotlin mudah dipelajari. Jadi kotlin ini merupakan salah satu bahasa tingkat tinggi atau biasa disebut *high level language* yang mana bahasa pemrograman jenis ini mudah dibaca oleh manusia berbeda dengan bahasa assembly.

Selain dari segi bahasanya, banyak pula resource atau sumber yang bisa dijadikan untuk belajar bahasa ini baik yang berbahasa inggris atau indonesia. Di youtube juga banyak tentang tutorial pemrograman Kotlin yang berbahasa Indonesia 🇮🇩

## Kekurangan

Disetiap hal didunia pasti memiliki suatu kelemahan, kotlin pun demikian berikut beberapa kelemahan kotlin, ini saya ambil dari beberapa sumber:

1. Kecepatan Kompilasi
Tidak semua *case* pada pemorgraman Kotlin itu cepat, namun ada beberapa *case* yang membuat bahasa ini lebih lamban tinimbang dengan bahasa Java.

2. Komunitas yang masih sedikit
Ini juga yang saya rasakan jadi memang komunitas kotlin masih terbilang sedikit, **dibanding** dengan bahasa bahasa yang lebih tua seperti PHP, Javascript, Java itu lebih banyak. Namun bukan berarti Kotlin sedikit banget, cukup banyak cuman hanya saja kalah dengan bahasa pemrograman lain seperti Komunitas Java, PHP, atau Javascript.

## Memulai Menggunakan Kotlin

Nah setelah itu untuk mulai menggunakan bahasa kotlin itu bisa menggunakan IDE seperti Intellij IDEA atau melalui terminal langsung. Untuk cara penggunaan IDE akan saya bahas di artikel tutorial berikutnya.

Kalau kalian tidak pengen ribet install environment untuk *ngoding* kotlin bisa pake cara dibawah.

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/pengenalan/image_UDtFCqH8a.png?updatedAt=1641102491291)

Yang kedua kalian bisa mengkode gitu via Web. yaitu melaui **play.kotlinlang.org** , situs ini merupakan official dari Jetbrain. Selain itu banyak lagi situs online yang bisa kalian gunakan untuk *ngoding* kotlin.

## Referensi

- https://glints.com/id/lowongan/kotlin-adalah/#.YdE21xNBwUo
- https://kotlinlang.org