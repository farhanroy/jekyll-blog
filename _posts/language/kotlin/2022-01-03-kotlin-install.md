---
layout: post
title:  "Tutorial Kotlin 2 : Menginstall kotlin dan IDE nya"
date:   2021-12-25 13:08:07 +0700
permalink: /tutorial/kotlin/install
categories: tutorial kotlin
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/kotlin_nkK7BWb0U.webp?updatedAt=1630209054609"
excerpt: "Meginstall kotlin dan menyiapkan environment nya untuk memulai pemrograman menggunakan bahasa kotlin"
hidden: true
comment: true
---

Setelah dikenalkan dengan bahasa kotlin, selanjutnya sebelum memulai memprogram menggunakan bahasa Kotlin tentunya kita menyiapkan *environment* nya dulu. Tak hanya menginstall kotlin di artikel ini juga akan membuat project pertama di **Kotlin**.

Baik, pertama kita menginstall software yang namanya **Intellij IDEA**

## Menginstall Intellij IDEA

Sebenarnya terserah mau menggunakan IDE apa, cuman saya sarankan untuk menggunakan Intellij IDEA karena cukup mudah dalam pemrograman kotlin dan tools yang disediakan lengkap apa lagi bagi pemula.

Silahkan download melalui link [ini](https://www.jetbrains.com/idea/download/#section=mac)

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/install/image_a_uyk2l6I.png?updatedAt=1641177826791)

Sebelum download pilih file sesuai Sistem Operasi yang akan kalian gunakan bisa Windows, Mac, atau Linux.

Ada 2 tipe dari software ini, ada yang **Ultimate** dan **Community**, kita pake yang community aja karena gratis dan tidak ada masa trial. Kalau kalian punya sedikit rejeki boleh pakai yang Ultimate, untuk fitur memang ada perbedaan cuman untuk ngoding yang nggak terlalu expert pake Community aja sudah cukup.

Untuk cara menginstall **Intellij Idea** tinggal next and next seperti menginstall pada software lainnya

Berikut tampilan ketika sudah dijalankan di desktop.

![](https://www.jetbrains.com/idea/img/screenshots/idea_overview_5_1@2x.png)

Setelah diinstall silahkan buka dan ikuti step dibawah ini.

## Memulai Project Kotlin
![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/install/image_QcpOq1Cfp.png?updatedAt=1641178304950)

Gambar diatas tampilan awal pada Intellij Idea, untuk membuat project baru maka klik Button **New Project**, lalu kalian akan diarahkan ke halaman seperti pada gambar dibawah ini

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/install/image_1__Wr5tWkZIe.png?updatedAt=1641178360999)

Di bagian ini jangan lupa untuk mencentang **Kotlin/JVM** karena kita mau membuat project `Kotlin`

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/install/image_2__HRtKtnWYm.png?updatedAt=1641178456837)

Lalu bagian ini isi nama project dan file path. File path ini merupakan folder dimana project kalian akan disimpan

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/install/image_3__bN-zsvOiY.png?updatedAt=1641178459609)

Tampilan diatas akan muncul ketika baru pertama kali membuat project. Untuk membuat file kotlin baru ikuti langkah di bawah ini

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/install/image_4__GWBEbTbm9.png?updatedAt=1641178724188)

Pada folder root project, pada folder **src**. Klik kanan lalu **New**->**Kotlin Class/File**. Lalu beri nama file nya. Disini saya beri nama "hello"

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/install/image_MPMBl0-Iq.png?updatedAt=1641178739338)

Setelah membuat sebuah file kotlin lalu ketikan kode dibawah ini:

```kotlin
fun main() {
    print("Hello World")
}
```

Setelah itu coba running dengan pergi ke menu **Run** -> **Run**.

Lalu file kotlin akan dijalankan oleh kompiler dan akan ditampilkan output nya seperti pada gambar dibawah ini

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/install/image_1__n4aL6D6YZ.png?updatedAt=1641178858054)

Nah itu dia tutorial singkat tentang kotlin kalau memang ada pertanyaan mengenai artikel ini silahkan saja komen melalui kolom dibawah.