---
layout: post
title:  "Tutorial Kotlin 2 : Tipe Data dan Variabel"
date:   2021-08-16 13:08:07 +0700
permalink: /tutorial/kotlin/tipe-data-dan-variabel
categories: tutorial kotlin
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-bahasa-kotlin/kotlin_nkK7BWb0U.webp?updatedAt=1630209054609"
excerpt: "Variable menjadi elemen terpentng dalam mempelajari suatu bahasa pemrograman. Itu terjadi karena variable menjadi wadah untuk menyimpan data, yang nantinya data tersebut dapat digunakan ketika program berjalan atau running."
hidden: true
comment: true
---

Variable menjadi elemen terpentng dalam mempelajari suatu bahasa pemrograman. Itu terjadi karena variable menjadi wadah untuk menyimpan data, yang nantinya data tersebut dapat digunakan ketika program berjalan atau *running*. 

## Apa itu Variabel ?
Apa sih variable itu ? Variable adalah tempat menyimpan sebuah data atau *value*. Kemudian nanti kita juga akan membahas tentang tipe data. Tipe data merupakan tipe atau jenis data yang ada apakah itu berupa angka, karakter, atau kata. Jika kalian masih bingung mengenai variable, kita analogikan sebagai kotak yang mana kotak tersebut berisi bola. Nah kotak adalah varibel dan bola adalah data.

Baik, langsung saja kita ke implementasi variable pada bahasa ``kotlin``. Untuk membuat variable pada kotlin, ada keyword khusus untuk **mendeklariskan** suatu variable yaitu ``val`` dan ``var``. Perhatikan contoh sintaks berikut:

```kotlin
val salam = "Hai"
```
> Ingat kotlin tidak memakai titik koma atau semicolon di akhir baris program

Kalian juga bisa mengganti data yang ada didalamnya.
```kotlin
var salam = "Hai"
salam = "Hello"
print(salam)  // Hello
```

Atau mengkombinasikan dengan variable lain
```kotlin
val x = 3

val y = 4

val z = x + y

print(z)
```

## Aturan Menulis Variabel di Kotlin
Ada beberapa aturan penulisan variabel di Kotlin yang sebaiknya ditaati agar valid dan tidak error. Berikut aturan - aturan dalam menulis variable:

1. Variabel kosong yang belum diberikan nilai wajib disebutkan tipe datanya.
2. Penulisan nama variabel menggunakan gaya CamelCase.
3. Nama variabel tidak boleh diawali dengan angka dan simbol
4. Nama variabel tidak boleh menggunakan simbol, kecuali garis bawah atau underscore.
5. Tipe data diawali dengan huruf kapital

## Tipe data
Untuk menentukan tipe data pada kotlin. Kalian perlu menambahkan keyword type datanya setelah nama variable dengan separator titik dua ( : ). Berikut contoh pengimplentasiannya
```kotlin
val salam: String = "Hai"
```
Contoh diatas merupakan tipe data string. Kotlin sendiri menyiapkan banyak tipe data, diantaranya:
- Double
- Float
- Int
- Long
- Short
- Byte
- Boolean
- String
- Characters
- Arrays

Untuk lebih lengkapnya kalian bisa mengunjungi dokumentasinya di [sini](https://kotlinlang.org/docs/reference/basic-types.html)

## Perbedaan ``val`` dan ``var``

Pada pemrograman **Kotlin** ada 2 tipe penulisan variabel, apa bedanya ? berikut saya paparkan lebih lanjut

1. val (Immutable reference) 

Yaitu Variabel yang dinyatakan menggunakan keyword ``val`` tidak dapat diubah begitu nilai ditetapkan. Hal ini mirip dengan variabel akhir pada java.

2. var (Mutable reference) 

Yaitu Variabel yang dideklarasikan dengan keyword ``var`` dapat diubah kemudian dalam program sesuai dengan variabel Java biasa.
contoh :

```kotlin 
var pelajaran ="matematika"
pelajaran = "ips"
```

Dari contoh diatas Variabel pelajaran dipindahkan ke pelajaran ips. Karena variabelnya dinyatakan menggunakan var, maka kode tersebut akan berjalan dengan baik. Apa bila program tadi mengguanakan ``val`` maka akan membuat error.

**Error:** *Val cannot be reassigned*

## Penutup

Nah itu dia penjelasan tentang variable di bahasa pemrograman kotlin. Selanjutnya akan dibahas tentang **null safety** karena ini materi sangat fundamental sekali. Seperti biasa, jika kalian belum mengerti tentang variable di kotlin silahkan mengunjungi dokumentasi resmi nya di https://kotlinlang.org/docs/reference/basic-types.html

## Referensi
- https://badoystudio.com/belajar-kotlin-lengkap-3-memahami-variabel-dan-tipe-data-dasar/
- https://www.petanikode.com/kotlin-variabel-tipe-data/
