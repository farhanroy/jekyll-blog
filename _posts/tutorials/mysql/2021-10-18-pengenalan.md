---
layout: post
title:  "Belajar Database MySQL untuk Pemula: Pengenalan"
date:   2021-10-18 13:08:07 +0700
permalink: /tutorial/mysql/pengenalan
categories: tutorial
categories: tutorial mysql
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-database-mysql/belajar_bahasa_kotlin_1_pengeNALAN_KOTLIN_Ogki6iUkKuR.png?updatedAt=1635577885019"
excerpt: MySQL adalah salah aplikasi database dimana data website nantinya akan disimpan. Meskipun terdapat berbagai aplikasi database sejenis, di bidang web programming MySQL tetap men- dominasi. MySQL bisa digunakan dengan gratis, namun aplikasi ini bukanlah database yang “seadanya”.
---

MySQL adalah salah aplikasi database dimana data website nantinya akan disimpan. Meskipun terdapat berbagai aplikasi database sejenis, di bidang web programming MySQL tetap men- dominasi. MySQL bisa digunakan dengan gratis, namun aplikasi ini bukanlah database yang “seadanya”.

## Apa itu Database ?
Mungkin pertama kalian perlu mengetahui apa sih database itu ? Jadi database atau basis data itu adalah Sekumpulan data yang terorganisasi atau bahasa kasarnya **terstruktur**. Pada jaman dulu data seperti data penjuaalan, data penggunaan produk, atau data penduduk itu masih tertulis dikertas, kelemahaannya adalah semakin banyak data maka semakin banyak kertas, nah untuk mencari data nya pun susah.

Belum lagi jika itu data yang perlu didistribusikan ini akan cukup memakan waktu. Seiring perkembangan teknologi, data mulai di **Digitalisasi** atau di bahasa pojok kampunge di di dekek dek memori, ini menyebabkan penyimpanan data menjadi lebih ringkas dan lebih tahan. 

Terus database itu gimana ?

Data yang tersimpan di komputer itu tercecer atau tidak teroganisir jadi dibentuklah database. **Spreadsheet** yang kita kenal itu termasuk database karena bisa menyimpan dan memanipulasi data, bahkan juga memvisualisasi data agar lebih mudah untuk dibaca.

## Apa itu Database model ?

Menurut [wikipedia](wikipedia.org) Database model adalah teori dasar seputar bagaimana data itu akan disimpan, disusun, dan dimanipulasi dalam sebuah sistem database.

Sebenarnya ada beberapa Database Model yang biasa digunakan seperti **flat model**, **hierarchical model**, **network model**, **relational model**, **object oriented model**, dan lain sebagainya. Namun yang MySQL pakai adalah Relational Model. Dari sekian model tadi, Model inilah yang paling banyak digunakan. 

## Apa itu Relational model ?

**Relation model** menyusun data ke dalam bentuk tabel yang saling terhubung. Tabel-tabel inilah yang nantinya menjadi sebuah database.

Konsep Relational Model dibuat oleh [Dr. Edgar F. Codd](http://en.wikipedia.org/wiki/Edgar_F._Codd) salah satu peneliti IBM dan ahli matematika pada tahun 1969. Konsep relational database model berasal dari 2 cabang ilmu matematika: **set theory** dan **first-order predicate logic**.

Relational model ini kita ibaratkan ada 2 tabel mahasiswa yang satu data mahasiswa yang satu lagi nilai IP Semester 1 - 3 dari tiap tiap mahasiwa. Jadi pada tabel data mahasiwa terdapat nim, nama, asal, dan jurusan. Dan di tabel nilai ada no, nim, ips1, ips2, dan ips3. Di tabel nilai terdapat nim dari masing masing nilai, untuk menunjukan pemilik nilai, nah nim ini memiliki hubungan atau relasi di data mahasiswa.

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/belajar-database-mysql/pengenalan/image_V_UmZQa_gAQ.png)
 
Biar lebih jelas coba lihat contoh gambar diatas.

## MySQL sebagai RDBMS

MySQL menjadi RDBMS (Relational Dabasase Management System), jadi mysql menjadi software untuk memanipulasi database dengan model Relational Database bukan database tapi RDBMS. Banyak sekali jenis RDBMS selain Mysql seperti Oracle, Sybase, Microsoft Access, Microsoft SQL Server, PostgreSQL dan juga MariaDB.

Mysql ini bersifat open source atau sumber terbuka. MySQL di support oleh ribuan programmer dari seluruh dunia, dan merupakan sebuah aplikasi RDBMS yang lengkap, cepat, dan reliabel. Mysql menggunakan bahasa SQL (Strukture Query Language) untuk manajemen databasenya, dan juga ada tool tool yang bisa tanpa harus query dengan SQL seperti phpmyadmin.

OK setelah ini langsung saja kita ketahui keunggulan dan kekurangannya

## Keunggulan dan Kekurangannya

Keunggulan dari MySQL sebagai berikut:
1. MySQL Merupakan Multi-user
2. Bisa Digunakan Dengan Spesifikasi Hardware Yang Rendah
3. DBMS yang Open Source

Disamping itu MySQL juga memiliki beberapa kelemahan:
1. Tidak support mobile app
2. Sulit Diaplikasikan dengan Database yang Besar

## Referensi
- Ebook Mysql Uncover (Andre Pratama)
- https://www.nesabamedia.com/15-kelebihan-dan-kekurangan-mysql-server-yang-perlu-diketahui/


