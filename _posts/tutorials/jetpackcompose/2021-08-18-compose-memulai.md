---
layout: post
title:  "Tutorial Jetpack compose: Memulai project"
date:   2021-08-16 13:08:07 +0700
permalink: /tutorial/jetpackcompose/memulai
categories: tutorial jetpackcompose
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/jetpack-compose_Owon8C0xX.webp?updatedAt=1630208853199"
excerpt: Memulai project baru dengan jetpack compose
---

Hai temen temen semua, kali ini saya akan membagikan kepada kalian, bagaimana memulai project **Jetpack Compose**. Ini merupakan bagian pertama dari seri Jetpack Compose Tutorial Indonesia. Baik langsung saja kita ke pembahasan.

## Introduction

![Jetpack Compose](https://ichi.pro/assets/images/max/724/1*V--60CT86PdmpTbP-Xyx-w.jpeg)

Jadi apa sih **Jetpack Compose** itu ? 
 
**Jetpack Compose** adalah seperangkat toolkit untuk UI untuk pengembangan aplikasi android yang intuitif. Konsepnya hampir sama dengan ``SwiftUI`` dan ``Flutter``. Dengan kata lain, saat kalian implemenatasi UI di android tidak lagi memakai ``xml`` tapi memakai **API** yang sudah disediakan oleh **Jetpack Compose** yang tentunya menggunakan bahasa ``Kotlin``. 

Toolkit UI ini mulai di kenalkan oleh tim android google pada Android Developer Summit 2019 lalu. Hingga saat artikel ini dibuat versi dari **Jetpack Compose** masih ``beta07``. Kabarnya, versi stable akan dirilis pada juli 2021 mendatang.

## Persiapan project
Sebelum membuat peoject baru jetpack compose, kalian harus persiapkan **Android Studio Canary terbaru**. Karena hingga saat artikel ini dibuat, jetpack compose hanya support versi Android Studio Canary. Kalau kalian belum punya download [di sini](). Jangan lupa untuk menginstall **Android SDK** nya juga.

Nah, kalau semua sudah terinstall dengan baik, langsung saja kita buat project **Jetpack Compose** nya. *Pertama*, create project

![Create new project](https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/memulai-project/create-project_XNcRpPn_o.png)

Setelah itu pilih **Empty Compose Activity**, Lalu klik finish. ini akan membuat activity yang menggunakan jetpack compose sebagai ``view``.

![Choose Activity](https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/memulai-project/choose-project_rlKtz2U70T-.png)

Terakhir tunggu hingga proses ``build gradle`` selesai.

## Struktur project

Struktur project jetpack compose secara default, terdapat satu activity dan sebuah paackage yang diberi nama ``ui``. Biasanya, saya menaruh file dari tiap screen di dalam package ini. Di dalam package ini terdapat lagi package ``theme``. 

![Project Structure](https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/memulai-project/project-structure_gEsh2pGIPW2.png)
 
Disini berisi konfigurasi dari tema aplikasi (karena jetpack compose tidak menggunakan xml dalam layoutingnya jadi disediakan pula theme dalam bentuk kotlin). Kalian bisa otak atik sendiri mengenai ``theming`` di jetpack compose.
## Preview
Di Android Studio Canary Artix Fox disediakan fitur **preview**. Dengan fitur ini, developer dapat mendapat preview dari composable funtction. Jadi kalian bisa lihat tampilan tanpa harus running terlebih dahulu.
<!-- Foto -->
Kalian hanya cukup menambahkan anotasi `@Preview` diatas composable function. seperti berikut
```kotlin
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MyApplicationTheme {
        Greeting("Android")
    }
}
```

## Composable Function

Sederhananya, sebuah fungsi yang menganotasikan composable ``@Composable``. Dengan anotasi tersebut menunjukan bahwa fungsi tersebut merupakan composable. ``@Composable`` dapat diimplementasika juga terhadap **lambda**. Composable function hanya dapat dipanggil didalam composable function. Seperti

```kotlin
@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
    More()
}

@Composable
fun More() {
    Text(text = "How are you")
}
```

``Composable`` hampir sama dengan ``Widget`` pada Flutter. Bedanya pada penempatanya saja, yang satu berupa anotasi yang satu lagi merupakan tipe data.

## Penutup
Nah itu dia temen - temen tutorial singkat mengenai memulai project jetpack compose. Kalian bisa dokumentasi lengkapnya di 
https://developer.android.com/jetpack/compose
