---
layout: post
title:  "Tutorial Jetpack compose: Modifier"
date:   2021-08-16 13:08:07 +0700
permalink: /tutorial/jetpackcompose/memulai
categories: tutorial jetpackcompose
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/jetpack-compose_Owon8C0xX.webp?updatedAt=1630208853199"
excerpt: Memulai project baru dengan jetpack compose
---

Hai temen - temen semua, pada kesempatan kali ini, saya akan membagikan tutorial menggunakan ``Modifier`` di Jetpack Compose. Sebelum itu itu lebih baik kita pahami dulu apa itu ``Modifier``.

## Apa itu Modifier ?
Semua komponen UI memiliki parameter ``Modifier``. ``Modifier`` memungkinkan kita untuk mengubah cara ``Composable`` disajikan, beberapa yang bisa Modifier lakukan diantaranya:

- Ubah perilaku dan tampilan ``Composable``
- Tambahkan deskripsi seperti label aksesibilitas
- Proses user input
- Menambahkan interaksi pengguna seperti membuat elemen ``clickable()``, ``scrollable()``, ``draggable()``, atau ``zoomable()``.

Jika kalian pernah mengembangkan aplikasi ``Flutter``, untuk membuat padding atau click, kalian perlu Widget tambahan untuk mendukung hal tersebut. Dengan menggunakan ``Modifier``, semua tersedia dalam satu metode dari tampilan hingga interaksi user. So, langsung saja kita ke percobaannya.

## Layout Modifier
Bagian ini merupakan elemen ``Modifier`` yang dapat mengubah tampilan dari ``Composable`` seperti size dan padding.

- **Modifier.width()** untuk mengatur lebar Composable.
```kotlin
@Composable
fun ModifierWidht(){
    Spacer(modifier = Modifier.width(100.dp))
}
```

- **Modifier.height()** untuk mengatur ketinggian / height Composable.

```kotlin
@Composable
fun ModifierHeight(){
    Spacer(modifier = Modifier.height(100.dp))
}
```
- **Modifier.size()** untuk mengatur lebar dan tinggi dari Composable.
```kotlin
@Composable
fun ModifierSize(){
    Spacer(modifier = Modifier.width(100.dp))
}
```
- **Modifier.fillMaxHeight()**
Ini akan mengatur height pada Composable ke ketinggian max yang tersedia. Ini mirip dengan **MATCH_PARENT** di ``xml`` layout.
```kotlin
@Composable
fun ModifierFillMax(){
    Spacer(modifier = Modifier.fillMaxHeight())
}
```
- **Modifier.fillMaxWidth()**
Ini akan mengatur lebar Composable ke lebar maksimum yang tersedia. Ini hampir sama dengan **MATCH_PARENT** di `xml` layout.
```kotlin
@Composable
fun ModifierFillMax(){
    Spacer(modifier = Modifier.fillMaxWidth())
}
```

- **Modifier.fillMaxSize()** mengatur tinggi/lebar Composable ke tinggi/lebar maksimum yang tersedia

```kotlin
@Composable
fun ModifierFillMax(){
    Spacer(modifier = Modifier.fillMaxSize())
}
```

- **Modifier.padding()**
Anda dapat menggunakan ``Modifier.padding`` untuk menyetel padding ke Composables, ada varian all, simetris, atau satu persatu dari tiap arah (top, left, right, bottom). Kalau penggunaan secara simetri seperti berikut ``Modifier.padding(horizontal = 8.dp, vertical = 16.dp)``

```kotlin
@Composable
fun ModifierPadding(){
    Spacer(
        modifier = Modifier.padding(
            start = 4.dp,
            top = 4.dp,
            bottom = 4.dp,
            end = 4.dp
        )
    )
}
```

## Draw Modifier
- **Modifier.background()**
Dengan modifier ini kalian dapat mengatur warna/bentuk dari 
background ``Composable``
 
<div style="text-align:center">

<img width="600" src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/modifier/modifier_background__9NdbWytfoI.png">

</div>

- **Modifier.border()**
Menambahkan border pada ``Composable``, border berisi ketebalan border, warna, shape, dan brush.

<div style="text-align:center">
<img width="600" src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/modifier/modifier_border_T4uKjkM5v.png">
</div>


- **Modifier.clip()**
Fungsi ini dapat membuat bentuk atau shape dari element ``Composable``.
 
<div style="text-align:center">
<img width="600" src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/modifier/modifier_clip_ua6X5-hjf.png">
</div>

## Penutup

Nah itu dia penerapan `Modifier`, sebenarnya masih banyak lagi fitur dari ``Modifier``, kalian bisa lihat sendiri di dokumentasi resminya https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier.
Untuk tutorial gesture modifier saya akan buatkan sendiri artikelnya. sekian
