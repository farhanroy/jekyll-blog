---
layout: post
title:  "Tutorial Jetpack compose: Textfield"
date:   2021-08-22 13:08:07 +0700
permalink: /tutorial/jetpackcompose/textfield
categories: tutorial jetpackcompose
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/jetpack-compose_Owon8C0xX.webp?updatedAt=1630208853199"
excerpt: Tampilan text field akan banyak digunakan pada case case seperti login, signup, atau input data lainnya. 
draft: false    
---

Dalam tutorial menulis Jetpack ini kita akan belajar cara membuat Textfield di aplikasi Android. TextField banyak digunakan untuk input teks yang dapat dibuat dengan banyak gaya. Berkat alat ini, pengguna akan dapat menulis teks di aplikasi Anda, misalnya, untuk mengisi form, menggunakan mesin telusur untuk melakukan penelusuran tertentu, aplikasi perpesanan, dan opsi lainnya. 

Jika kalian ingin menyesuaikan bidang teks sehingga pengguna memiliki pengalaman yang lebih baik saat menjelajahi aplikasi Anda, yang harus Anda lakukan sangat sederhana, Anda hanya perlu mengatur warna latar belakang teks jika Anda ingin mengubahnya. Selain itu, Anda akan memiliki opsi untuk memilih warna kontras untuk kursor dalam langkah yang sangat sederhana . Ini setara dengan EditText dari sistem Android View.

### Tipe text field
- **Filled**
- **Outlined**

## Let's Code
Langsung saja kita ke buat aplikasi yang menggunakan text field menggunakan `Jepack Compose`.

**Langkah 1:** Buat aplikasi android di android studio

**Langkah 2:** Ikuti langkah untuk menyiapkan Jetpack Compose dengan Android Studio

**Langkah 3:** Tambahkan Bidang Teks di MainActivity.kt

### Simpel Text Field
Baik kita buat dulu text field biasa menggunakan jetpack compose. Berikut potongan kode composable functionnya.

```kotlin
@Composable
fun TextFieldDemo() {
    Column(Modifier.padding(16.dp)) {
        val textState = remember { mutableStateOf(TextFieldValue()) }
        TextField(
            value = textState.value,
            onValueChange = { textState.value = it }
        )
    }
}
```

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/textfield/Screen_Shot_2021-10-08_at_10.37.51_tWu2sRgbR.png?updatedAt=1633664478980)


Di kode tersebut terdapat penggunaan state (belum saya bahas). Sekedar spoiler, jadi penggunaan state di jetpack compose menggunakan `remember`. State ini bisa menyimpan nilai sementara, kenapa kok nggak pakek variabel saja ? nah itu akan saya bahas di artikel tentang penggunaan state di jetpack compose.  

> Note: State harus berada di scope composable function karena kalau diluarnya bisa terjadi **error**
 
### Mengahandle perubahan value 
Kalau sebelumnya hanya menyimpan value dari text field. Sekarang ke pengaksesan state tersebut.

```kotlin
@Composable
fun TextFieldDemo() {
    Column(Modifier.padding(16.dp)) {
        val textState = remember { mutableStateOf(TextFieldValue()) }
        TextField(
            value = textState.value,
            onValueChange = { textState.value = it }
        )
        Text("The textfield has this text: " + textState.value.text)
    }
}
```

### Membuat placeholder
Kalian juga bisa menambahkan sebuah placeholder. **Placeholder** pada textfield di jetpack compose cukup menambahkan konten di properti `placeholder = {}`. Properti itu bertipe **Unit** jadi konten harus dibungkus *kurung kurawal*.
```kotlin
TextField(
    // below line is used to get
    // value of text field,
    value = textState.value,
    // below line is used to get value in text field
    // on value change in text field.
    onValueChange = { textState.value = it },
    // below line is used to add placeholder
    // for our text field.
    placeholder = {
        Text(text = "Enter something")
    },
)
```
![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/textfield/Screen_Shot_2021-10-08_at_13.40.40_8AqEapVev.png?updatedAt=1633675913996)
### Keyboard Option

Ada beberapa kasus yang membutuhkan jenis keyboard tertentu. Misal pada case OTP kode perlu keyboard yang hanya angka saja, atau mungkin form email yang tipe keyboard `@`. Disini saya mencontohkan bagaimana menggunakan KeyboardType number.

```kotlin
TextField(
    // below line is used to get
    // value of text field,
    value = textState.value,
    // below line is used to get value in text field
    // on value change in text field.
    onValueChange = { textState.value = it },
    keyboardOptions = KeyboardOptions(
        // keyboard options is used to modify
        // the keyboard for text field.
        keyboardType = KeyboardType.Text,
        // below line is use for capitalization
        // inside our text field.
        capitalization = KeyboardCapitalization.Words,
        // below line is to enable auto
        // correct in our keyboard.
        autoCorrect = false
    ),
    // modifier is use to add padding
    // to our text field.
    modifier = Modifier
        .padding(all = 8.dp)
        .fillMaxWidth(),
)
```
![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/textfield/Screenshot_1633675408_CsKcM6FUiJZ.png?updatedAt=1633675915955)

### Styling textfield

Ada properti `textStyle`. Properti ini memungkinkan untuk menstyling text pada textfield. Saya mencoba mengganti text pada textfield menjadi merah.

```kotlin
TextField(
    // below line is used to get
    // value of text field,
    value = textState.value,
    // below line is used to get value in text field
    // on value change in text field.
    onValueChange = { textState.value = it },

    // below line is use to give
    // max lines for our text field.
    maxLines = 4,
    // single line boolean is use to avoid
    // textfield entering in multiple lines.
    singleLine = false,
    textStyle = TextStyle(
        color = Color.Red,

        // below line is used to add font
        // below line is use to change font family.
        fontFamily = FontFamily.SansSerif
    ),
)
```
Berikut hasilnya setelah dirunning
![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/textfield/Screen_Shot_2021-10-08_at_13.45.15_Rd6p8Kcxf.png?updatedAt=1633675914887)


### Menambah ikon di awal
`leadingIcon` dapat memposisikan konten yang ada didalamnya berada diawal sebelum text dari textfield itu sendiri. Properti ini bertipe `Unit` jadi harus didalam kurung kurawal.



```kotlin
TextField(
    value = textState.value,
    onValueChange = { textState.value = it },
    label = {
        Text(text = "Text Field With Leading Icon")
    },
    // leading icon is use to add icon
    // at the start of text field.
    leadingIcon = {
        // In this method we are specifying
        // our leading icon and its color.
        Icon(
            imageVector = Icons.Default.Email,
            contentDescription = "image",
            tint = Color.Blue
        )
    },
)
```

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/textfield/Screen_Shot_2021-10-08_at_10.38.15_I-SxG3-9q.png?updatedAt=1633664479534)

### Trailing icon

```kotlin
TextField(
    value = textState.value,
    onValueChange = { textState.value = it },
    label = {
        Text(text = "Text Field With Trailing Icon")
    },
    //trailing icon is use to add icon
    // at the end of text field.
    trailingIcon = {
        // In this method we are specifying
        // our leading icon and its color.
        Icon(
            imageVector = Icons.Default.Email,
            contentDescription = "image",
            tint = Color.Blue
        )
    },
)
```
![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/textfield/Screen_Shot_2021-10-08_at_10.38.22_idgzPLoTu.png?updatedAt=1633664479817)

### Outlined Textfield

Selain dari textfield seperti diatas, ada text field dengan style yang berbeda. `OutlinedTextField` merupakan text field yang memiliki border yang mengelilingi dengan background secara default **putih**. Properti - propertinya pun sama dengan textfield yang sebelumnya.

```kotlin
val textState = remember { mutableStateOf(TextFieldValue()) }
OutlinedTextField(
    value = textState.value,
    onValueChange = {
        textState.value = it
    },
    label = {
        Text(text = "Outline Text Field")
    },
)
```

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/textfield/Screen_Shot_2021-10-08_at_10.38.28_T9mI8-BId.png?updatedAt=1633664480621)

### Handling error di textfield

Handle error textfield di jetpack compose. Menambah pesan error ketika data yang diisikan ke text field tidak sesuai. Saya membuat sebuah state untuk menyimpan apakah data dari textfield masih valid atau tidak.

```kotlin
var text by rememberSaveable { mutableStateOf("") }
var isError by rememberSaveable { mutableStateOf(false) }

fun validate(text: String) {
    isError = text.count() < 5
}
TextField(
    value = text,
    onValueChange =
    {
        text = it
        isError = false

    },
    singleLine = true,
    label =
    { Text(if (isError) "Email*" else "Email") },
    isError = isError,
    keyboardActions = KeyboardActions
    { validate(text) },
    modifier = Modifier.semantics
    {
        // Provide localized description of the error
        if (isError) error("Email format is invalid.")
    }
)
```

## Kesimpulan

Nah itu dia tentang pembuatan textfield di jetpack compose. Ada 2 jenis textfield **Outlined Textfield** dan **Outlined Textfield**. Keduanya hanya berbeda pada tampilannya saja secara fungsional tetap sama. Disini ada penggunaan *state*, yang akan saya bahas di artikel selanjutnya. 

## Referensi
-