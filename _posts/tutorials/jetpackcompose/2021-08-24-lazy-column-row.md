---
layout: post
title:  "Tutorial Jetpack compose: LazyColumn dan LazyRow"
date:   2021-08-23 13:08:07 +0700
permalink: /tutorial/jetpackcompose/lazy-column-lazy-row
categories: tutorial jetpackcompose
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/jetpack-compose_Owon8C0xX.webp?updatedAt=1630208853199"
excerpt: Bagaimana membuat horizontal dan vertical scrolling pada Jetpack Compose
---

Hai guys, pada tutorial kali ini saya membahas tentang vertical atau horizontal scrolling pada **Jetpack Compose**. Ini menjadi penting, karena pada kenyataannya banyak dibutuhkan scroll element untuk menampilkan list atau banyak gambar seperti halnya pada aplikasi whatsapp atau youtube

## Introduction
Seperti yang sudah dijelaskan diawal tadi tentang ``LazyColumn`` dan ``LazyRow``. Sebenarnya untuk membuat scrolling element cukup hanya dengan ``Row`` atau ``Column`` (tergantung pada axis yang diinginkan). Seperti contoh Composable berikut:
```kotlin
@Composable
fun MessageList(messages: List<Message>) {
    Column {
        messages.forEach { message ->
            MessageRow(message)
        }
    }
}
```
Jika kalian perlu menampilkan item yang besar dan kompleks (atau daftar panjang yang tidak diketahui), menggunakan tata letak seperti Column dapat menyebabkan masalah performa, karena semua element akan dikomposisi dan ditata baik terlihat atau tidak.
 
Compose menyediakan kumpulan komponen yang hanya mengomposisi dan menata letak item yang terlihat di area pandang komponen. Komponen ini meliputi ``LazyColumn`` dan ``LazyRow``. Seperti namanya, perbedaan antara ``LazyColumn`` dan ``LazyRow`` adalah pada orientasi tata letak item dan **scroll**. *LazyColumn* menghasilkan daftar scroll **vertikal**, dan *LazyRow* menghasilkan daftar scroll **horizontal**.

## LazyColumn
Composable ``LazyColumn`` mempunyai fungsi **item()** untuk menampilkan satu component dan **items()** untuk menampilkan banyak component
```kotlin
LazyColumn {
    // Add a single item
    item {
        Text(text = "First item")
    }

    // Add 5 items
    items(5) { index ->
        Text(text = "Item: $index")
    }

    // Add another single item
    item {
        Text(text = "Last item")
    }
}
```

## LazyRow
Sama dengan ``LazyColumn``, mempunyai fungsi **item()** dan **items()**.

```kotlin
LazyRow {
    item {
        Text("First Item")
    }
    items(5) { index ->
        Text(text = "Item: $index")
    }
}
```

## Mengatur jarak elemen
Untuk menambahkan spasi di antara item, Anda dapat menggunakan Arrangement.spacedBy(). Contoh di bawah ini menambahkan 4.dp spasi di antara setiap item:


```kotlin
LazyColumn(
    verticalArrangement = Arrangement.spacedBy(4.dp),
) {
    // ...
}
```

Demikian pula untuk LazyRow:

```kotlin
LazyRow(
    horizontalArrangement = Arrangement.spacedBy(4.dp),
) {
    // ...
}
```


## Penutup

Nah itu dia tutorial singkat bagaimana membuat scrollable element secara horizontal dan vertikal dengan menggunakan ``LazyRow`` dan ``LazyColumn``. Seperti biasa kalau kalian pengen tahu lebih detail mengenai materi ini silahkan kunjungi di [sini](https://developer.android.com/reference/kotlin/androidx/compose/foundation/lazy/package-summary?hl=id#LazyRow(androidx.compose.ui.Modifier,androidx.compose.foundation.lazy.LazyListState,androidx.compose.foundation.layout.PaddingValues,kotlin.Boolean,androidx.compose.foundation.layout.Arrangement.Horizontal,androidx.compose.ui.Alignment.Vertical,androidx.compose.foundation.gestures.FlingBehavior,kotlin.Function1))

Sekian 👋👋👋

## Referensi
Berikut referensi saya untuk membuat artikel ini :
- https://developer.android.com/jetpack/compose/lists?hl=id

- https://foso.github.io/Jetpack-Compose-Playground/foundation/lazyrow/

- https://foso.github.io/Jetpack-Compose-Playground/foundation/lazycolumn/
