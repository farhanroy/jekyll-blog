---
layout: post
title:  "Tutorial Jetpack compose: Row dan Column"
date:   2021-08-22 13:08:07 +0700
permalink: /tutorial/jetpackcompose/row-column
categories: tutorial jetpackcompose
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/jetpack-compose-tutorial/jetpack-compose_Owon8C0xX.webp?updatedAt=1630208853199"
excerpt: Basic layouting pada Jetpack Compose
---

Mungkin terlintas di pikiran kalian bagaimana sih nampilin element urut kebawah atau urut kesamping. Nah pada tutorial kali ini, kalian akan belajar mengenai **Layouting** pada jetcpack compose.

## Introduction
Jetpack Compose menyediakan ``API`` untuk menampilkan elemen kebawah atau kesamping. Berbeda dengan ``xml``, untuk layoutingnya hanya menggunakan satu ``view`` tidak dispesifikan sendiri - senidiri. Metode hampir sama penerapannya di **Flutter**.

![](https://developer.android.com/images/jetpack/compose/layout-column-row-box.png)

`Row()` untuk menampilkan elemen kesamping, sedangkan ``Column()`` menampilkan elemen urut kebawah. Ini merupakan basic layouting pada jetpack compose, sangat penting untuk di pelajari guna membuat UI yang sempurna.Nah langsung saja ke pembahasan lebih lanjut.

## Row
Seperti yang telah saya jelaskan di intro, ``Row`` untuk menampilkan child atau elemen di dalamnya berupa Baris atau kesamping. 
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_lVoKXK6tbwD.png">
</div>

``Row`` meliki parameter pendukung, seperti ``modifier``, ``horizontalArrangement``, ``verticalAlignment``. Ketiganya memiliki fungsi - fungsi yang bisa memebantu kalian dalam menyusun UI.

### horizontalArrangement
Parameter ini berfungsi untuk menentukan urutan horizontal dari child si ``Row`` nya. Apakah susuannya diawal, diakhir, atau ditengah. Seperti contoh composable berikut:
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_arrangement_53NVEcD4tIy.png">
</div>

Paramater ini akan diisi oleh objek ``Arrangement`` yang memiliki  element - element:
- Arrangement.Start
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_start_iuGH3nlmD.png">
</div>

- Arrangement.End
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_end_KmZZpkTO4.png">
</div>

- Arrangement.Center
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_center_t220yv-89Ze.png">
</div>

- Arrangement.SpaceBetween
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_space_between_Hu3DL7DmrDh.png">
</div>

- Arrangement.SpaceEvenly
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_space_evenly_28VwfXUbZ.png">
</div>

- Arrangement.SpaceArround
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_space_arround_jyO693efb.png">
</div>

### verticalAlignment
Pada parameter ini berfungsi untuk mengatur alignment pada child row secara vertikal. Seperti contoh berikut

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_alignment_PgYPAKcn2yR.png">
</div>

Paramater ini akan diisi oleh objek ``Alignment`` yang memiliki  element - element:
* Top
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_align_top_H9Ja71f85WV.png">
</div>

* CenterVertically
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_align_center_DtBvCvxDM.png">
</div>

* Bottom
<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/row_align_bottom_WgnX3bRcQ.png">
</div>

## Column
Kalau tadi menampilkan element berbaris sekarang menampilkan element kolom atau urut kebawah secara vertikal. Berikut contoh kode penerapan ``Column``.

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_12pTviv0fB7.png">
</div>

Column memiliki height **MATCH_PARENT** secara default, namun kalian juga bisa menentukan sendiri dengan nilai ``Dp()`` atau di **WRAP_CONTENT** dengan cara mengisi parameter ``Modifier.requiredHeight``

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_min_sOuLuSBp_.png">
</div>

``Column`` meliki parameter pendukung, seperti ``modifier``, ``horizontalArrangement``, ``verticalAlignment``. Ketiganya memiliki fungsi - fungsi yang bisa memebantu kalian dalam menyusun UI.

### verticalArrangement
Parameter ini berfungsi untuk menentukan urutan vertical dari child si ``Column`` nya. Apakah susuannya diawal, diakhir, atau ditengah. Seperti contoh composable berikut

<div style="text-align:center">
<img src="">
</div>

Paramater ini akan diisi oleh objek ``Arrangement`` yang memiliki  element - element:
- Arrangement.Start

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_top_u3ufavGUY.png">
</div>

- Arrangement.End

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_bottom_mztd6m_rp.png">
</div>

- Arrangement.Center

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_center_51udqu1F5i4.png">
</div>

- Arrangement.SpaceBetween

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_space_between_VlBgjE3NMLZ.png">
</div>

- Arrangement.SpaceEvenly

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_space_evenly_uBxO9qnF2Gj.png">
</div>

- Arrangement.SpaceArround

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_space_arround_faR8gyZSt.png">
</div>

### horizontalAlignment
Pada parameter ini berfungsi untuk mengatur alignment pada child Column secara horizontal. Seperti contoh berikut

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_align_4_WR2nfhxxR.png">
</div>

Paramater ini akan diisi oleh objek ``Alignment`` yang memiliki  element - element:
* Start

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_align_start_th3V0azDH.png">
</div>

* CenterHorizontally

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_align_center_9FAWBE6kCFA.png">
</div>

* End

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/jetpack-compose-tutorial/column_row/column_align_end_eeXm_3c4T.png">
</div>


## Penutup

Nah itu dia penjelasan dan cara penerapan dari ``Row`` dan ``Column``, Sebenarnya masih ada lagi composable yang bisa menampilkan baris atau kolom dan dapat discroll yang akan saya bahas di tutorial selanjutnya.

Seperti biasa, jika kalian masih belum menegerti tentang materi ini silahkan kunjungi dokumentasi resminya di https://developer.android.com/reference/kotlin/androidx/compose/foundation/layout/package-summary

Sekian :)
