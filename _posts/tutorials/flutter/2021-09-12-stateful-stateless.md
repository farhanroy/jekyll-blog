---
layout: post
title:  "Tutorial Flutter Indonesia: Stateful & Stateless Widget"
date:   2021-09-12 14:08:07 +0700
permalink: /tutorial/flutter/stateful-stateless
categories: tutorial flutter
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/flutter-tutorial-indonesia/flutter_LvoUxcHo8a.webp?updatedAt=1630208888851"
excerpt: "Container hampir sama dengan tag div pada html, widget ini biasanya dijadikan penampung untuk widget lain. Banyak sekali kegunaannya"
---


Hampir semua example code flutter menggunakan Stateless dan Stateful Widget. Namun apakah perbedaan dari keduanya ? maka dari itu saya akan bahas perbedaan kedua widget ini.

## Stateless Widget
Stateless merupakan widget yang di-build hanya dengan konfigurasi yang telah diinisiasi sejak awal. Jadi Stateless Widget adalah Widget yang tidak akan pernah berubah.
 
Misal kita mempunyai gambar di aplikasi kita. Apabila kita hanya ingin menampilkan gambar maka widget yang paling tepat untuk digunakan adalah stateless, karena penerapannya sama dengan konsep stateless widget.

![](https://ik.imagekit.io/u8uufhbnoej/blog/flutter-tutorial-indonesia/stateful-stateless/unnamed_ExnvGuOB23ma.png)

## Stateful Widget
Widget ini merupakan kebalikan dari widget Stateless, widget ini memungkinkan kita untuk membuat widget secara interaktif. Jadi misal kita membutuhkan widget yang dapat berubah - ubah seperti tombol favorit, animasi ataupun yang lainnya.

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/flutter-tutorial-indonesia/stateful-stateless/statefull_l5tHsEP_2F.png?updatedAt=1630208415949&tr=w-1200,h-630,fo-auto)

Kode diatas merupakan contoh sederhana dari penggunaan stateful widget yang mana terdiri dari 2 kelas. Kelas pertama ini digunakan untuk menciptakan object bertipe State yaitu kelas kedua dalam metode 'createState', data di dalam kelas ini tidak bisa diubah, datanya bersifat final dan nilainya diberikan melalui konstruktor, sama seperti Stateless, kelas ini dibuang dan digantikan ketika ada perubahan data dan Widget baru perlu dibuat ulang.
Untuk menset state di widget menggunakan setState()

![](https://ik.imagekit.io/u8uufhbnoej/blog/flutter-tutorial-indonesia/stateful-stateless/setstate_N1f1vQnYNePk.png)

Pada kelas yang kedua ini akan terjadi banyak proses ketika kita menggunakan state. Jadi ketika kita men set state maka flutter akan merender ulang widget widget yang ada di method build
Stateful widget juga memiliki lifecycle atau daur hidup dari si widget dari pertama di build hingga di destroy. konsep ini hampir sama dengan konsep lifecycle activity/fragment pada Android Native.
Berikut lifecycle yang tersedia di flutter:

- createState()
Saat kita membangun Stateful Widget baru, maka widget akan langsung memanggil createState()

- initState()
metode pertama yang dipanggil setelah Widget dibuat. Ini setara dengan onCreate () dan viewDidLoad ()

- didChangeDependencies()
Metode ini dipanggil segera setelah initState() saat pertama kali widget dibuat. Jika StatefulWidgets kita bergantung dari InheritedWidget, ia akan memanggil lagi jika diperlukan perubahan.

- build()
Metode ini adalah metode yang sangat penting. Di sini ia merelay Widget kita untuk di render dan dipanggil tepat setelah didChangeDependencies (). Semua komponen UI di render disini dan akan dipanggil setiap kali UI perlu di render karena menggambar lagi adalah operasi yang murah.

- didUpdateWidget()
Mungkin Lifecycle ini sering jarang kita jumpai, tetapi seperti namanya, ini akan dipanggil setelah Widget induk melakukan perubahan dan perlu menggambar ulang UI

- deactivate()
Flutter memanggil metode ini setiap kali ia menghapus objek Status ini dari Widget Tree. Dalam beberapa kasus, Si flutter akan memasukkan kembali objek ke bagian lain dari widget tree.

- dispose()
Yang ini sangat penting dan dipanggil ketika objek ini dan Statusnya dihapus dari widget tree secara permanen dan tidak akan pernah di build lagi.
Lifecycle ini adalah satu-satunya yang kita butuhkan untuk berhenti Stream controller, destroy animasi, dll. Sama halnya dengan initState.


