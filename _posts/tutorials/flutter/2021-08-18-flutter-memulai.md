---
layout: post
title:  "Tutorial Flutter Indonesia: Memulai Project"
date:   2021-08-16 13:08:07 +0700
permalink: /tutorial/flutter/memulai
categories: tutorial flutter
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/flutter-tutorial-indonesia/flutter_LvoUxcHo8a.webp?updatedAt=1630208888851"
excerpt: "Menyiapkan segala keperluan untuk mulai mengembangkan aplikasi dengan flutter"
---

Sebelum kita membuat aplikasi pada flutter, kita harus menyiapkan environmentnya terlebih dahulu. Berikut langkah - langkahnya:

> NOTE: "Sebelum install flutter SDK kalian harus install Android SDK terlebih dahulu"
1. Download Flutter SDK disini dan Intellij Idea disini

2. Setelah mendownload keduanya, Ekstrak file Zip Flutter SDK yang sudah di download di direktori kesukaan kalian disini saya menaruhnya di C://

3. Lalu setting Path Variable Flutter dan Dart nya. Di windows, setting path variable dengan cara :
    - cari di search bar windows, ’ environment ’ dan kemudian pilih Edit environment variables for your account
    - Di kotak System Path  cari item Path kemudian pilih dengan mengklik 2 kali
    - Setelah itu akan keluar kotak dialog, kemudian tekan button new, isilah dengan path flutter yang telah kalian ekstrak contoh: **C://flutter/bin/** 
    - Lalu tambahkan path dart juga dengan mengklik tombol new, kemudian isilah dengan path dart. Contoh “ C:\flutter\bin\cache\dart-sdk\bin ”
    - Setelah itu tekan ok ok ok
    - Jika laptop atau pc kalian ber OS lain silahkan cek link ini 
4 Test apakah flutter anda terinstall di cmd dengan perintah ``flutter doctor``

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/flutter-tutorial-indonesia/persiapan-project/flutter-doctor_CEmO2Ju0oq.png">
</div>

5. Setelah Flutter dan Dart sudah terdaftar di path variable windows, Install Intellij Idea yang sudah di Download

6. Untuk menginstall plugin flutter dan dart pada intellij idea, Buka Softwarenya lalu Pergi ke menu **File->Setting->Plugin** lalu search ‘flutter’ di search bar.

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/flutter-tutorial-indonesia/persiapan-project/plugin_XezghYlsIen.png">
</div>

7. Lalu install dengan menekan tombol install. Intellij akan meminta kalian untuk menginstall dart plugin juga, jadi kalau ada dialog seperti itu tekan yes atau ok saja
8. Nah, agar aplikasi kalian bisa berjalan di laptop, diperlukan yang namanya Android Emulator, cara install android emulator silahkan kunjungi link ini: https://developer.android.com/studio/run/managing-avds

Untuk sistem operasi yang kalian gunakan linux, kalian bisa ikuti tutorial berikut: https://flutter.dev/docs/get-started/install/linux

Di MacOs ikuti link berikut: https://flutter.dev/docs/get-started/install/macos
IDE untuk Flutter

Ada beberapa pilihan IDE (Integrated Development Environment), yakni Visual Studio Code, Intellij Studio, dan Android Studio. Nah, tutorial di atas memakai Intellij Idea.

Kalau kalian pengen menggunakan VScode untuk develop flutter agar terasa lebih ringan, maka cukup install plugin flutter di Extensions nya si VScode. Setelah install plugin, restart VScode agar plugin berjalan lancar.

<div style="text-align:center">
<img src="https://ik.imagekit.io/u8uufhbnoej/blog/flutter-tutorial-indonesia/persiapan-project/vscode-plugin_tBg2fv98JV.png">
</div>

Selanjutnya kita akan belajar bagaimana membuat aplikasi pertama dengan Flutter



