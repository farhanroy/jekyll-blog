---
layout: post
title:  "Tutorial Flutter Indonesia: Widget Container"
date:   2021-09-12 13:08:07 +0700
permalink: /tutorial/flutter/container
categories: tutorial flutter
image: "https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/flutter-tutorial-indonesia/flutter_LvoUxcHo8a.webp?updatedAt=1630208888851"
excerpt: "Container hampir sama dengan tag div pada html, widget ini biasanya dijadikan penampung untuk widget lain. Banyak sekali kegunaannya"
---

Salah satu widget yang paling mudah di customize. Container menyediakan banyak sekali properti untuk kita membuat UI sedemikian rupa agar sesuai dengan keinginan. Widget Container hampir sama dengan tag Div di HTML.

Dengan widget ini kita dapat membuat macam - macam UI seperti Shape yang bundar, Shadow, Color yang bergradien dan banyak hal lain yang bisa diimplementasikan dengan widget Container.

Bagaimana penerapannya? OK langsung saja kita buat contoh implementasi Container di flutter.

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/flutter-tutorial-indonesia/basic-layouting/1_7obLVrjqc.png?updatedAt=1632137475336)

Dari contoh kode di atas, Container harus atau memiliki properti yang wajib diisi yaitu width dan height. 

## Margin 

Margin digunakan untuk mengatur jarak dari elemen ke elemen lainnya. di flutter ada beberapa tipe. Berikut pembagiannya :

- **EdgeInsets.all**

Membuat semua bagian (atas, bawah, kanan dan kiri) memiliki nilai. Jadi kalau kita mengatur margin dengan tipe ini maka widget akan memiliki margin dari segala arah

→ EdgeInsets.all(double value)

- **EdgeInsets.only**

Membuat hanya sebagian memiliki nilai. Artinya kalau kita ingin menset satu offset saja misal top maka offset lainnya akan bernilai nol (default value)

→ EdgeInsets.only({double left: 0.0, double top: 0.0, double right: 0.0, double bottom: 0.0})

- **EdgeInsets.symetric**

Membuat margin dengan arah horizontal(kanan dan kiri) dan vertikal(atas dan bawah). Jika kita mengeset nilai horizontal maka widget yang yang diberi margin akan memiliki jarak di offset kanan dan kiri, begitu pun vertikal.

→ EdgeInsets.symmetric({double vertical: 0.0, double horizontal: 0.0})

- **EdgeInsets.fromLTRB**

Membuat semua bagian (atas, bawah, kanan dan kiri) memiliki nilai. Bedanya dengan all adalah value dari setiap offset harus diisi kalau tidak diisi maka akan error

→ EdgeInsets.fromLTRB(double left, double top, double right, double bottom)

## Box Decoration
Inilah yang saya bilang tadi, bahwa kita bisa meng custom Container. Banyak properti yang disediakan oleh Container, saya hanya akan menjelaskan beberapa properti saja untuk lebih lengkapnya silahkan cek [link](https://api.flutter.dev/flutter/painting/BoxDecoration-class.html) 

![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/flutter-tutorial-indonesia/basic-layouting/2_Vxb_HdkzV.png?updatedAt=1632137475219)

Baik, dari kode diatas saya beri border pada widget dengan menambahkan Border.all() di properti border dengan warna hitam dan ketebalan 3. 


![](https://ik.imagekit.io/u8uufhbnoej/blog/tutorials/flutter-tutorial-indonesia/basic-layouting/3__NH398Ews.png?updatedAt=1632137475462)

Baik, dari kode diatas saya beri border pada widget dengan menambahkan Border.all() di properti border dengan warna hitam dan ketebalan 3. 


Kita juga bisa mengatur border radius agar terlihat lebih tumpul. Ini hampir sama penerapannya dengan CSS.
